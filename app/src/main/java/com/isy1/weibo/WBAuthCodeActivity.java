package com.isy1.weibo;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.isy1.R;
import com.isy1.util.Constant;
import com.isy1.util.Utility;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMessage;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;

public class WBAuthCodeActivity extends AppCompatActivity implements IWeiboHandler.Response {

    private IWeiboShareAPI mWeiboShareAPI = null;
    private Utility utility;

    public static final String SCOPE =
            "email,direct_messages_read,direct_messages_write,"
                    + "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
                    + "follow_app_official_microblog," + "invitation_write";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utility = new Utility(this);

        mWeiboShareAPI = WeiboShareSDK.createWeiboAPI(WBAuthCodeActivity.this, Constant.WEIBO_APP_KEY);
        mWeiboShareAPI.registerApp();

        if (savedInstanceState != null) {
            mWeiboShareAPI.handleWeiboResponse(getIntent(), this);
        }

        if (mWeiboShareAPI.isWeiboAppSupportAPI()) {
            sendSingleMessage(true, true);
//            shareText();
        } else {
            showDownload();
//            Toast.makeText(this, "Plea", Toast.LENGTH_SHORT).show();
//            Intent viewIntent =
//                    new Intent("android.intent.action.VIEW",
//                            Uri.parse("https://play.google.com/store/apps/details?id=com.adeebhat.rabbitsvilla"));
//            startActivity(viewIntent);
        }
    }

    private void showDownload() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Please download Weibo first for sharing...");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();
    }


    @Override
    public void onResponse(BaseResponse baseResponse) {
        if (baseResponse != null) {
            Log.i("response", "" + baseResponse.errCode + "  " + baseResponse.reqPackageName);
            if (baseResponse.errCode == 0) {
                Toast.makeText(WBAuthCodeActivity.this, "Posted Successfully.", Toast.LENGTH_SHORT).show();
            }
            finish();
        }
    }

    private void sendSingleMessage(boolean hasText, boolean hasImage) {
        WeiboMessage weiboMessage = new WeiboMessage();
        if (hasText) {
            weiboMessage.mediaObject = getTextObj();
        }
        if (hasImage) {
            weiboMessage.mediaObject = getImageObj();
        }

        SendMessageToWeiboRequest request = new SendMessageToWeiboRequest();
        request.transaction = String.valueOf(System.currentTimeMillis());
        request.message = weiboMessage;

        mWeiboShareAPI.sendRequest(WBAuthCodeActivity.this, request);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mWeiboShareAPI.handleWeiboResponse(intent, this);
    }

    private TextObject getTextObj() {
        TextObject textObject = new TextObject();
        textObject.text = "this is text";
        return textObject;
    }

    /**
     * 创建图片消息对象。
     *
     * @return 图片消息对象。
     */
    private ImageObject getImageObj() {
        ImageObject imageObject = new ImageObject();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        imageObject.setImageObject(bitmap);
        return imageObject;
    }

    private void shareText() {

        //初始化微博的分享消息ShareUtil
        WeiboMultiMessage weiboMultiMessage = new WeiboMultiMessage();
        weiboMultiMessage.textObject = getTextObj();
        //初始化从第三方到微博的消息请求
        SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
        request.transaction = buildTransaction("sinatext");
        request.multiMessage = weiboMultiMessage;
        allInOneShare(request);
    }

    public String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis())
                : type + System.currentTimeMillis();
    }

    private void allInOneShare(SendMultiMessageToWeiboRequest request) {

        AuthInfo authInfo = new AuthInfo(this, Constant.WEIBO_APP_KEY, Constant.REDIRECT_URL, SCOPE);
        SsoHandler mSsoHandler = new SsoHandler(this, authInfo);
        mSsoHandler.authorize(new WeiboAuthListener() {
            @Override
            public void onComplete(Bundle bundle) {
                Log.i("done", "done");
            }

            @Override
            public void onWeiboException(WeiboException e) {
                e.printStackTrace();
            }

            @Override
            public void onCancel() {

            }
        });
        Oauth2AccessToken accessToken = AccessTokenKeeper.readAccessToken(getApplicationContext());
        String token = "";
        if (accessToken != null) {
            token = accessToken.getToken();
        }
        mWeiboShareAPI.sendRequest(this, request, authInfo, token, new WeiboAuthListener() {

            @Override
            public void onWeiboException(WeiboException arg0) {
                arg0.printStackTrace();
            }

            @Override
            public void onComplete(Bundle bundle) {
                Oauth2AccessToken newToken = Oauth2AccessToken.parseAccessToken(bundle);
                AccessTokenKeeper.writeAccessToken(getApplicationContext(), newToken);
                Toast.makeText(getApplicationContext(), "onAuthorizeComplete token = " + newToken.getToken(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(WBAuthCodeActivity.this, "cancel", Toast.LENGTH_LONG).show();
            }
        });
    }
}
