package com.isy1;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.isy1.auth.LoginActivity;
import com.isy1.util.AllAPICall;
import com.isy1.util.Constant;
import com.isy1.util.SessionUtil;
import com.isy1.util.Utility;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private SessionUtil sessionUtil;
    public static List<String> arrYear, arrVehicleModel;
    private Utility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionUtil = new SessionUtil(this);
        utility = new Utility(this);
        arrYear = new ArrayList<>();
        arrVehicleModel = new ArrayList<>();
        setYearModelValues();
    }

    public void setYearModelValues() {
        if (utility.checkInternetConnection()) {
            new AllAPICall(this, new HashMap<String, String>(), null, new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getInt("status") == 1) {
                            JSONArray jsonArrYear = jsonObject.getJSONArray("modelYear");
                            for (int i = 0; i < jsonArrYear.length(); i++) {
                                arrYear.add(jsonArrYear.get(i).toString());
                            }
                            JSONArray arrModel = jsonObject.getJSONArray("vehicleList");
                            for (int j = 0; j < arrModel.length(); j++) {
                                arrVehicleModel.add(arrModel.get(j).toString());
                            }
                            arrYear.add(getResources().getString(R.string.text_select_year) + "<font color='red'>*</font>");
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (sessionUtil.isContain("iUserId"))
                                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                                else
                                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                                finish();
                            }
                        }, Constant.SPLASH_TIME);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_MODEL_YEAR);
        } else {
            showOkDialog(getResources().getString(R.string.msg_internet_connection));
        }
    }

    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                setYearModelValues();
            }
        });
        adb.show();
    }
}


