package com.isy1.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.WebviewActivity;
import com.isy1.auth.ChangePasswordActivity;
import com.isy1.auth.ProfileActivity;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;
import com.isy1.weibo.WBAuthCodeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SettingActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private TextView textHeader, textMyProfile, textChangePassword, textAboutUs, textContactUs, textTerms,
            textPrivacy, textShare;
    private SwitchCompat swOnOff;
    private ImageView imgWeiBo, imgFb;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_settings);
        initControl();
    }

    private void initControl() {
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(R.string.lbl_settings);

        textMyProfile = (TextView) findViewById(R.id.textMyProfile);
        textChangePassword = (TextView) findViewById(R.id.textChangePassword);
        textAboutUs = (TextView) findViewById(R.id.textAboutUs);
        textContactUs = (TextView) findViewById(R.id.textContactUs);
        textTerms = (TextView) findViewById(R.id.textTerms);
        textPrivacy = (TextView) findViewById(R.id.textPrivacy);
        textShare = (TextView) findViewById(R.id.textShare);

        swOnOff = (SwitchCompat) findViewById(R.id.swOnOff);
        swOnOff.setOnCheckedChangeListener(this);

        textMyProfile.setOnClickListener(this);
        textChangePassword.setOnClickListener(this);
        textAboutUs.setOnClickListener(this);
        textContactUs.setOnClickListener(this);
        textTerms.setOnClickListener(this);
        textPrivacy.setOnClickListener(this);
        textShare.setOnClickListener(this);

        imgWeiBo = (ImageView) findViewById(R.id.imgWeiBo);
        imgFb = (ImageView) findViewById(R.id.imgFb);

        imgWeiBo.setOnClickListener(this);
        imgFb.setOnClickListener(this);

        if (utility.checkInternetConnection())
            getNotificationOnOff();
    }

    private void sharingIntent() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>This is the text that will be shared.</p>"));
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }

    public void finishActivity(View view) {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textMyProfile)) {
            startActivity(new Intent(this, ProfileActivity.class));
        } else if (v.equals(textChangePassword)) {
            startActivity(new Intent(this, ChangePasswordActivity.class));
        } else if (v.equals(textAboutUs)) {
            startActivity(new Intent(this, WebviewActivity.class).putExtra("pageid", "2"));
        } else if (v.equals(textContactUs)) {
            startActivity(new Intent(this, ContactUsActivity.class));
        } else if (v.equals(textTerms)) {
            startActivity(new Intent(this, WebviewActivity.class).putExtra("pageid", "5"));
        } else if (v.equals(textPrivacy)) {
            startActivity(new Intent(this, WebviewActivity.class).putExtra("pageid", "3"));
        } else if (v.equals(imgWeiBo)) {
            startActivity(new Intent(this, WBAuthCodeActivity.class));
        } else if (v.equals(imgFb)) {
            facebookSharing();
        } else if (v.equals(textShare)) {
            sharingIntent();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (utility.checkInternetConnection())
                setNotificationOnOff("y");
        } else {
            if (utility.checkInternetConnection())
                setNotificationOnOff("n");
        }
    }

    private void setNotificationOnOff(String str) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("eIsAcceptPush", str);
        stringHashMap.put("pushType", "change");

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_PUSH_NOTIFICATION_ON_OFF);
    }

    private void getNotificationOnOff() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("pushType", "view");

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        if (jsonObject.getString("eIsAcceptPush").equalsIgnoreCase("y")) {
                            swOnOff.setChecked(true);
                        } else {
                            swOnOff.setChecked(false);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_PUSH_NOTIFICATION_ON_OFF);
    }

    private void facebookSharing() {

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i("share response", result.toString());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            Uri path = Uri.parse("android.resource://com.isy1/" + R.mipmap.ic_launcher);
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("ISY1")
                    .setContentDescription("ISY1 testing facebook sharing")
                    .build();

            shareDialog.show(linkContent);

//        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
//
//        SharePhoto photo = new SharePhoto.Builder()
//                .setBitmap(image)
//                .build();
//        SharePhotoContent content = new SharePhotoContent.Builder()
//                .addPhoto(photo)
//                .build();
//
//        ShareDialog shareDialog = new ShareDialog(this);
//        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
        }
    }
}
