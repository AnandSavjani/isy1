package com.isy1.setting;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class ContactUsActivity extends BaseActivity implements View.OnClickListener {

    private EditText editMessage;
    private TextView btnSubmit;
    private TextView textHeader;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        initControl();
    }

    private void initControl() {
        stringHashMap = new HashMap<>();
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(R.string.lbl_contact_us);

        editMessage = (EditText) findViewById(R.id.editMessage);

        btnSubmit = (TextView) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    //methods

    public void finishActivity(View view) {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnSubmit)) {
            if (TextUtils.isEmpty(editMessage.getText().toString())) {
                editMessage.setError("Please enter your message");
                editMessage.requestFocus();
            } else {
                if (utility.checkInternetConnection())
                    contactUsApiCall();
            }
        }
    }

    private void contactUsApiCall() {
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("txMessage", getTextStr(editMessage));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        finish();
                    }
                    Toast.makeText(ContactUsActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_CONTACT_US);
    }
}