package com.isy1.setting;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FeedbackActivity extends BaseActivity implements View.OnClickListener {

    private TextView textHeader, textPost;
    private RadioGroup rgDelivery, rgCommunication;
    private RatingBar rbRating;
    private EditText editComment;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initControl();
    }

    private void initControl() {
        textHeader = (TextView) findViewById(R.id.textHeader);
        textPost = (TextView) findViewById(R.id.textPost);
        textHeader.setText(R.string.lbl_feedback);

        textPost.setOnClickListener(this);

        rgDelivery = (RadioGroup) findViewById(R.id.rgDelivery);
        rgCommunication = (RadioGroup) findViewById(R.id.rgCommunication);

        rbRating = (RatingBar) findViewById(R.id.rbRating);

        editComment = (EditText) findViewById(R.id.editComment);

        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            DrawableCompat.setTint(rbRating.getProgressDrawable(), ContextCompat.getColor(this, R.color.color_green));
        } else {
            LayerDrawable stars = (LayerDrawable) rbRating.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(ContextCompat.getColor(this, R.color.color_green), PorterDuff.Mode.SRC_ATOP);
        }
    }

    void checkValidation() {
        if (rbRating.getRating() <= 0) {
            utility.showOkDialog("Please give your valuable Rating.");
        } else {
            feedBackApiCall();
        }
    }

    void feedBackApiCall() {
        RadioButton rbCommunication = (RadioButton) rgCommunication.findViewById(rgCommunication.getCheckedRadioButtonId());
        RadioButton rbDelivery = (RadioButton) rgDelivery.findViewById(rgDelivery.getCheckedRadioButtonId());

        stringHashMap = new HashMap<>();
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("eDelivery", rbDelivery.getText().toString());
        stringHashMap.put("eCommunication", rbCommunication.getText().toString());
        stringHashMap.put("fRating", "" + rbRating.getRating());
        stringHashMap.put("txComment", getTextStr(editComment));
        stringHashMap.put("iOrderId", getIntent().getStringExtra("iOrderId"));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        Toast.makeText(FeedbackActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        utility.showOkDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_FEEDBACK);
    }

    public void finishActivity(View view) {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textPost)) {
            if (utility.checkInternetConnection())
                checkValidation();
        }
    }
}
