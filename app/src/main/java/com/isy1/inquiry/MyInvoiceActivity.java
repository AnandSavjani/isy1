package com.isy1.inquiry;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.adapter.MyInvoiceAdapter;
import com.isy1.bean.OrderBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyInvoiceActivity extends BaseActivity {

    private ListView lvInvoice;
    private MyInvoiceAdapter inquiryAdapter;
    private TextView textHeader, textNoResult;
    private List<OrderBean> listInvoice;
    private ImageView imgSearch;
    private HashMap<String, String> stringHashMap;
    private String strSearch = "";
    int nextPos, flag = 0;
    private EditText searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_invoice);
        initControl();
    }

    private void initControl() {
        listInvoice = new ArrayList<>();

        textHeader = (TextView) findViewById(R.id.textHeader);
        textNoResult = (TextView) findViewById(R.id.textNoResult);
        textHeader.setText(R.string.lbl_my_invoice);

        searchView = (EditText) findViewById(R.id.searchView);
        searchView.setOnEditorActionListener(onEditorActionListener);
        searchView.setOnTouchListener(touchListener);

        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgSearch.setVisibility(View.VISIBLE);
        imgSearch.setOnClickListener(onClickListener);

        lvInvoice = (ListView) findViewById(R.id.lvInvoice);
        inquiryAdapter = new MyInvoiceAdapter(this, listInvoice);
        lvInvoice.setAdapter(inquiryAdapter);
        lvInvoice.setOnScrollListener(onScrollListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            nextPos = 0;
            listInvoice.clear();
            myInvoiceApiCall(0);
        }
    }

    public void finishActivity(View view) {
        onBackPressed();
    }

    private void myInvoiceApiCall(int start) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("pageStart", "" + start);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());
        stringHashMap.put("search", strSearch);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Gson gson = new Gson();
                    if (jsonObject.getInt("status") == 1) {
                        flag = 0;
                        nextPos = nextPos + 10;
                        JSONArray arrResult = jsonObject.getJSONArray("result");
                        for (int i = 0; i < arrResult.length(); i++) {
                            listInvoice.add(gson.fromJson(arrResult.getJSONObject(i).toString(), OrderBean.class));
                        }
                        textNoResult.setVisibility(View.GONE);
                        lvInvoice.setVisibility(View.VISIBLE);
                        inquiryAdapter.notifyDataSetChanged();
                    } else {
                        flag = -1;
                        textNoResult.setVisibility(View.VISIBLE);
                        lvInvoice.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_INVOICES);
    }

    //listener

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (searchView.isShown()) {
                searchView.setVisibility(View.GONE);
                textHeader.setVisibility(View.VISIBLE);
                utility.hideSoftKeyboard(MyInvoiceActivity.this);
                searchView.setText("");
            } else {
                searchView.setVisibility(View.VISIBLE);
                textHeader.setVisibility(View.GONE);
                utility.openSoftKeyboard(MyInvoiceActivity.this, searchView);
                searchView.requestFocus();
            }
        }
    };
    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE && lvInvoice.getLastVisiblePosition() >= nextPos - 1 && flag != -1) {
                if (utility.checkInternetConnection()) {
                    myInvoiceApiCall(nextPos);
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                utility.hideSoftKeyboard(MyInvoiceActivity.this);
                strSearch = v.getText().toString();
                onResume();
            }
            return false;
        }
    };

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_RIGHT = 2;
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (searchView.getRight() - searchView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    strSearch = "";
                    onResume();
                    searchView.setText("");
                    utility.hideSoftKeyboard(MyInvoiceActivity.this);
                    searchView.clearFocus();
                    return true;
                }
            }
            return false;
        }
    };
}
