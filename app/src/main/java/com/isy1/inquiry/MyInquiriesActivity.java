package com.isy1.inquiry;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.util.CustomSearchView;
import com.isy1.util.NonSwipeableViewPager;

public class MyInquiriesActivity extends BaseActivity implements View.OnClickListener, TextView.OnEditorActionListener {

    private NonSwipeableViewPager viewPager;
    private PagerSlidingTabStrip tabs;
    private LinearLayout mTabsLinearLayout;
    private TextView textHeader;
    private FragmentPagerInquiriesAdapter fragmentStatePagerAdapter;
    private ImageView imgSearch;
    private CustomSearchView searchView;
    private MyHistoryFragment myHistoryAll, myHistoryCancel;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_inquiries);
        initControl();
    }

    private void initControl() {
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(getIntent().getStringExtra("header"));

        url = getIntent().getStringExtra("url");

        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgSearch.setVisibility(View.VISIBLE);
        imgSearch.setOnClickListener(this);

        searchView = (CustomSearchView) findViewById(R.id.searchView);
        searchView.setOnEditorActionListener(this);
        searchView.setOnTouchListener(touchListener);

        fragmentStatePagerAdapter = new FragmentPagerInquiriesAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.inquiries), getIntent().getStringExtra("header"));
        viewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(fragmentStatePagerAdapter);

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(viewPager);
        tabs.setTextColor(R.color.color_black);

        mTabsLinearLayout = ((LinearLayout) tabs.getChildAt(0));
        changeTabColor(0);
        tabs.setOnPageChangeListener(onPageChangeListener);
    }

    public void finishActivity(View view) {
        onBackPressed();
    }

    //*******method

    private void changeTabColor(int position) {
        for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
            if (i == position) {
                tv.setTextColor(Color.WHITE);
                tv.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green));
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.uparrow);
            } else {
                tv.setTextColor(Color.BLACK);
                tv.setBackgroundColor(ContextCompat.getColor(this, R.color.color_light_gray));
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
    }

    //*******listener

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            changeTabColor(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    @Override
    public void onClick(View v) {
        if (v.equals(imgSearch)) {
            if (searchView.isShown()) {
                searchView.setVisibility(View.GONE);
                textHeader.setVisibility(View.VISIBLE);
                utility.hideSoftKeyboard(MyInquiriesActivity.this);
                searchView.setText("");
            } else {
                searchView.setVisibility(View.VISIBLE);
                textHeader.setVisibility(View.GONE);
                utility.openSoftKeyboard(MyInquiriesActivity.this, searchView);
                searchView.requestFocus();
            }
        }
    }

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_RIGHT = 2;
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (searchView.getRight() - searchView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    searchView.setText("");
                    utility.hideSoftKeyboard(MyInquiriesActivity.this);
                    searchView.clearFocus();
                    searchFunctionality("");
                    return true;
                }
            }
            return false;
        }
    };

    private void searchFunctionality(String query) {
        if (myHistoryAll != null && viewPager.getCurrentItem() == 0) {
            myHistoryAll.searchForAll(query);
        } else if (myHistoryCancel != null && viewPager.getCurrentItem() == 1) {
            myHistoryCancel.searchForAll(query);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            utility.hideSoftKeyboard(MyInquiriesActivity.this);
            searchFunctionality(v.getText().toString());
            return true;
        }
        return false;
    }

    public class FragmentPagerInquiriesAdapter extends FragmentStatePagerAdapter {

        private String title[];
        private String header;

        public FragmentPagerInquiriesAdapter(FragmentManager fm, String title[], String header) {
            super(fm);
            this.title = title;
            this.header = header;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                myHistoryAll = MyHistoryFragment.newInstance(position, header, url);
                return myHistoryAll;
            } else {
                myHistoryCancel = MyHistoryFragment.newInstance(position, header, url);
                return myHistoryCancel;
            }
        }

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }
    }
}
