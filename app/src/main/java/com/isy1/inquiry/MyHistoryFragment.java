package com.isy1.inquiry;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.clans.fab.FloatingActionButton;
import com.google.gson.Gson;
import com.isy1.R;
import com.isy1.adapter.MyHistoryAdapter;
import com.isy1.bean.MyHistoryBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.SessionUtil;
import com.isy1.util.Utility;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyHistoryFragment extends Fragment implements TextView.OnEditorActionListener, AbsListView.OnScrollListener {

    int position;
    private SwipeMenuListView lvMyInquiries;
    private MyHistoryAdapter inquiryAdapter;
    private LinearLayout llFilter;
    private Utility utility;
    private FloatingActionButton fab;
    private List<MyHistoryBean> listMyInquiries;
    private String header, filterType;
    private HashMap<String, String> stringHashMap;
    private SessionUtil sessionUtil;
    private TextView textNoResult;
    int nextPos, flag = 0;
    private String strSearch = "";
    String url;

    public static MyHistoryFragment newInstance(int position, String header, String url) {
        MyHistoryFragment f = new MyHistoryFragment();
        Bundle b = new Bundle();
        b.putInt("position", position);
        b.putString("header", header);
        b.putString("url", url);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stringHashMap = new HashMap<>();
        sessionUtil = new SessionUtil(getActivity());
        listMyInquiries = new ArrayList<>();

        position = getArguments().getInt("position");
        header = getArguments().getString("header");
        url = getArguments().getString("url");

        if (position == 0) {
            filterType = "all";
        } else {
            filterType = "canceled";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_main, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        utility = new Utility(getActivity());

        textNoResult = (TextView) view.findViewById(R.id.textNoResult);

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

        llFilter = (LinearLayout) view.findViewById(R.id.llFilter);
        llFilter.setVisibility(View.GONE);

        lvMyInquiries = (SwipeMenuListView) view.findViewById(R.id.lvInquiry);
        lvMyInquiries.setOnItemClickListener(onItemClickListener);
        lvMyInquiries.setOnScrollListener(this);

        inquiryAdapter = new MyHistoryAdapter(getActivity(), listMyInquiries);
        lvMyInquiries.setAdapter(inquiryAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            nextPos = 0;
            listMyInquiries.clear();
            myInquiriesApiCall(nextPos);
        }
    }

    public void searchForAll(String query) {
        strSearch = query;
        onResume();
    }

    //methods

    private void myInquiriesApiCall(final int start) {
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("filterType", filterType);
        stringHashMap.put("pageStart", "" + start);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());
        stringHashMap.put("search", strSearch);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Gson gson = new Gson();
                    if (jsonObject.getInt("status") == 1) {
                        flag = 0;
                        nextPos = nextPos + 10;
                        JSONArray arrResult = jsonObject.getJSONArray("result");
                        for (int i = 0; i < arrResult.length(); i++) {
                            MyHistoryBean myHistoryBean = gson.fromJson(arrResult.getJSONObject(i).toString(), MyHistoryBean.class);
                            if (jsonObject.getString("message").contains("Inquiry")) {
                                myHistoryBean.label = "Inquiry ID";
                            } else {
                                myHistoryBean.label = "Order ID";
                            }
                            listMyInquiries.add(myHistoryBean);
                        }
                        textNoResult.setVisibility(View.GONE);
                        lvMyInquiries.setVisibility(View.VISIBLE);
                        lvMyInquiries.setSelectionFromTop(start - 1, 0);
                    } else {
                        flag = -1;
                        if (listMyInquiries.isEmpty()) {
                            lvMyInquiries.setVisibility(View.GONE);
                            textNoResult.setVisibility(View.VISIBLE);
                        }
                    }
                    inquiryAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
    }

    //listeners

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (header.equalsIgnoreCase("my inquiries")) {
                Intent intent = new Intent(getActivity(), InquiryDetailsActivity.class).putExtra("header", "Inquiry Details");
                intent.putExtra("inquiryId", listMyInquiries.get(position).orderId);
                startActivity(intent);

            } else {
                Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
                intent.putExtra("header", "Order Details");
                intent.putExtra("iOrderId", listMyInquiries.get(position).orderId);
                intent.putExtra("from", "history");
                startActivity(intent);
            }
        }
    };

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            utility.hideSoftKeyboard(getActivity());
            strSearch = v.getText().toString();
            onResume();
            return true;
        }
        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_IDLE && lvMyInquiries.getLastVisiblePosition() >= nextPos - 1 && flag != -1) {
            if (utility.checkInternetConnection()) {
                myInquiriesApiCall(nextPos);
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }
}
