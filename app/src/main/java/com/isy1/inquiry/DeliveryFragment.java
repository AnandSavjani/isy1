package com.isy1.inquiry;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.isy1.R;
import com.isy1.adapter.OrderAdapter;
import com.isy1.bean.OrderBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.Constant;
import com.isy1.util.SessionUtil;
import com.isy1.util.Utility;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DeliveryFragment extends Fragment {

    private int pos;
    private SwipeMenuListView lvDelivery;
    private LinearLayout llFilter;
    private Utility utility;
    private HashMap<String, String> stringHashMap;
    private SessionUtil sessionUtil;
    int nextPos, flag = 0;
    private TextView textNoResult;
    private OrderAdapter orderAdapter;
    private String strSearch = "";
    private List<OrderBean> listDelivery;
    private Gson gson;

    public static DeliveryFragment newInstance(int position) {
        DeliveryFragment f = new DeliveryFragment();
        Bundle b = new Bundle();
        b.putInt("position", position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pos = getArguments().getInt("position");
        Log.i("position", "" + pos);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_main, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gson = new Gson();
        sessionUtil = new SessionUtil(getActivity());
        utility = new Utility(getActivity());
        listDelivery = new ArrayList<>();

        textNoResult = (TextView) view.findViewById(R.id.textNoResult);

        llFilter = (LinearLayout) view.findViewById(R.id.llFilter);
        llFilter.setVisibility(View.GONE);

        lvDelivery = (SwipeMenuListView) view.findViewById(R.id.lvInquiry);

        orderAdapter = new OrderAdapter(getActivity(), listDelivery);
        lvDelivery.setAdapter(orderAdapter);
        lvDelivery.setOnItemClickListener(onItemClickListener);
        lvDelivery.setOnScrollListener(onScrollListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            listDelivery.clear();
            nextPos = 0;
            getDeliveredApiCall(0);
        }
    }

    //methods

    private void getDeliveredApiCall(final int start) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("pageStart", "" + start);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());
        stringHashMap.put("search", strSearch);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    if (jsonObject.getInt("status") == 1) {
                        flag = 0;
                        nextPos = nextPos + 10;
                        JSONArray arrResult = jsonObject.getJSONArray("result");

                        for (int i = 0; i < arrResult.length(); i++) {
                            listDelivery.add(gson.fromJson(arrResult.getJSONObject(i).toString(), OrderBean.class));
                        }
                        orderAdapter.notifyDataSetChanged();
                        lvDelivery.setSelectionFromTop(start - 1, 0);
                        lvDelivery.setVisibility(View.VISIBLE);
                        textNoResult.setVisibility(View.GONE);
                    } else {
                        flag = -1;
                        lvDelivery.setVisibility(View.GONE);
                        textNoResult.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_DELIVERED);
    }

    //listener

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
            intent.putExtra("position", pos);
            intent.putExtra("header", getResources().getString(R.string.lbl_delivered_details));
            intent.putExtra("iOrderId", listDelivery.get(position).orderId);
            intent.putExtra("from", "");
            startActivity(intent);
        }
    };

    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE && lvDelivery.getLastVisiblePosition() >= nextPos - 1 && flag != -1) {
                if (utility.checkInternetConnection()) {
                    getDeliveredApiCall(nextPos);
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    public void searchForDelivered(String query) {
        strSearch = query;
        onResume();
    }
}
