package com.isy1.inquiry;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.isy1.R;
import com.isy1.adapter.OrderAdapter;
import com.isy1.bean.OrderBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.Constant;
import com.isy1.util.SessionUtil;
import com.isy1.util.Utility;
import com.isy1.util.onAcceptListener;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderFragment extends Fragment {

    int pos;
    private SwipeMenuListView listView;
    private OrderAdapter orderAdapter;
    private LinearLayout llFilter;
    private Utility utility;
    private HashMap<String, String> stringHashMap;
    private SessionUtil sessionUtil;
    int nextPos, flag = 0;
    private List<OrderBean> listOrders;
    private TextView textNoResult;
    private SearchView searchDelivery;
    private String strSearch = "";

    public static OrderFragment newInstance(int position) {
        OrderFragment f = new OrderFragment();
        Bundle b = new Bundle();
        b.putInt("position", position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        pos = getArguments().getInt("position");
//        Log.i("position", "" + pos);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_main, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stringHashMap = new HashMap<>();
        sessionUtil = new SessionUtil(getActivity());
        listOrders = new ArrayList<>();
        utility = new Utility(getActivity());

//        searchDelivery = (SearchView) getActivity().findViewById(R.id.searchView);
//        searchDelivery.setOnQueryTextListener(onQueryTextListener);

        textNoResult = (TextView) view.findViewById(R.id.textNoResult);

        llFilter = (LinearLayout) view.findViewById(R.id.llFilter);
        llFilter.setVisibility(View.GONE);

        listView = (SwipeMenuListView) view.findViewById(R.id.lvInquiry);
        listView.setMenuCreator(menuCreator);

        orderAdapter = new OrderAdapter(getActivity(), listOrders);
        listView.setAdapter(orderAdapter);
        listView.setOnItemClickListener(onItemClickListener);
        listView.setOnMenuItemClickListener(onMenuItemClickListener);
        listView.setOnScrollListener(onScrollListener);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            listOrders.clear();
            nextPos = 0;
            orderListApiCall(0);
        }
    }

    // methods

    private void cancelOrder(String orderId, final int pos) {
        stringHashMap.clear();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("iOrderId", orderId);
        stringHashMap.put("orderType", "cancel");
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        listOrders.remove(pos);
                        orderAdapter.notifyDataSetChanged();
                    }
                    Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_ORDERS);
    }

    private void orderListApiCall(final int start) {
        stringHashMap.clear();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("orderType", "view");
        stringHashMap.put("pageStart", "" + start);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());
        stringHashMap.put("search", strSearch);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Gson gson = new Gson();
                    if (jsonObject.getInt("status") == 1) {
                        flag = 0;
                        nextPos = nextPos + 10;
                        JSONArray arrResult = jsonObject.getJSONArray("result");
                        for (int i = 0; i < arrResult.length(); i++) {
                            listOrders.add(gson.fromJson(arrResult.getJSONObject(i).toString(), OrderBean.class));
                        }
                        textNoResult.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                    } else {
                        flag = -1;
                        textNoResult.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                    }
                    orderAdapter.notifyDataSetChanged();
                    listView.setSelectionFromTop(start - 1, 0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_ORDERS);
    }

    // listeners

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
            intent.putExtra("position", pos);
            intent.putExtra("header", getResources().getString(R.string.lbl_order_details));
            intent.putExtra("iOrderId", listOrders.get(position).orderId);
            intent.putExtra("from", "");
            startActivity(intent);
        }
    };

    SwipeMenuListView.OnMenuItemClickListener onMenuItemClickListener = new SwipeMenuListView.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
            switch (index) {
                case 0:
                    utility.showConfirmationDialog("Are you sure you want to cancel the Order?", new onAcceptListener() {
                        @Override
                        public void onAccept(boolean isTrue) {
                            cancelOrder(listOrders.get(position).orderId, position);
                        }
                    });
                    break;
            }
            return false;
        }
    };

    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE && listView.getLastVisiblePosition() >= nextPos - 1 && flag != -1) {
                if (utility.checkInternetConnection()) {
                    orderListApiCall(nextPos);
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    private SwipeMenuCreator menuCreator = new SwipeMenuCreator() {
        @Override
        public void create(SwipeMenu menu) {
            SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
            deleteItem.setWidth(utility.dp2px(90));
            deleteItem.setIcon(R.drawable.cancle);
            menu.addMenuItem(deleteItem);
        }
    };

    SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            searchDelivery.clearFocus();
            strSearch = query;
            onResume();
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };

    public void searchForOrder(String query) {
        strSearch = query;
        onResume();
    }


}
