package com.isy1.inquiry;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.MapActivity;
import com.isy1.R;
import com.isy1.SplashActivity;
import com.isy1.adapter.HintAdapter;
import com.isy1.bean.VehicleDetailBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;
import com.wrapp.floatlabelededittext.FloatLabeledEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class PostInquiryActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, View.OnTouchListener {

    private EditText editEmail, editUserName, editpContactName, editpContactNum, editpAddress,
            editpZipcode, editdTotalDistance, editdContactName, editdContactNum, editdAddress, editdZipcode,
            editAnticipateDate, editInstruction, editUserNumber;

    private TextView textHeader;
    private Spinner spUnit;
    private ArrayAdapter<String> adapterModel, adapterUnit;
    private List<String> arrUnit;
    private TextView btnSave;
    private LinearLayout llVehicleBox;
    private HashMap<String, String> stringHashMap;
    private StringBuffer sbModelYear, sbModel, sbVin;
    private String strType = "add";
    private List<VehicleDetailBean> listVehicleModel;
    private double pLat = 0, pLong = 0, dLat = 0, dLong = 0;
    public HintAdapter adapterYear;
    private FloatLabeledEditText fetDeliver;
    private String inquiryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_inquiry);
        initControl();
    }

    private void initControl() {
        stringHashMap = new HashMap<>();
        arrUnit = new ArrayList<>();
        listVehicleModel = new ArrayList<>();

        inquiryId = getIntent().getStringExtra("inquiryId");

        editEmail = (EditText) findViewById(R.id.editEmail);
        editUserName = (EditText) findViewById(R.id.editUserName);
        editUserNumber = (EditText) findViewById(R.id.editUserNumber);
        editpContactName = (EditText) findViewById(R.id.editpContactName);
        editpContactNum = (EditText) findViewById(R.id.editpContactNum);
        editpAddress = (EditText) findViewById(R.id.editpAddress);
        editpZipcode = (EditText) findViewById(R.id.editpZipcode);
        editdTotalDistance = (EditText) findViewById(R.id.editdTotalDistance);
        editdContactName = (EditText) findViewById(R.id.editdContactName);
        editdContactNum = (EditText) findViewById(R.id.editdContactNum);
        editdAddress = (EditText) findViewById(R.id.editdAddress);
        editdZipcode = (EditText) findViewById(R.id.editdZipcode);

        editAnticipateDate = (EditText) findViewById(R.id.editAnticipateDate);
        editInstruction = (EditText) findViewById(R.id.editInstruction);
        editpContactNum.setOnEditorActionListener(onEditorActionListener);
        editdContactNum.setOnEditorActionListener(onEditorActionListener);

        fetDeliver = (FloatLabeledEditText) findViewById(R.id.fetDeliver);
        fetDeliver.setOnClickListener(this);

        editAnticipateDate.setFocusableInTouchMode(false);
        editAnticipateDate.setOnClickListener(this);

        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(getIntent().getStringExtra("header"));

        btnSave = (TextView) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        spUnit = (Spinner) findViewById(R.id.spUnits);
        llVehicleBox = (LinearLayout) findViewById(R.id.llVehicleBox);

        Collections.addAll(arrUnit, getResources().getStringArray(R.array.units));

        adapterModel = new ArrayAdapter<>(this, R.layout.simple_spinner_item, SplashActivity.arrVehicleModel);
        adapterUnit = new ArrayAdapter<>(this, R.layout.simple_spinner_item, arrUnit);
        adapterUnit.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUnit.setAdapter(adapterUnit);

        utility.setHintColor(editEmail, editEmail.getHint().toString());
        utility.setHintColor(editpAddress, editpAddress.getHint().toString());
        utility.setHintColor(editdAddress, editdAddress.getHint().toString());
        utility.setHintColor(editAnticipateDate, editAnticipateDate.getHint().toString());

        editpAddress.setOnTouchListener(this);
        editdAddress.setOnTouchListener(this);

        if (inquiryId != null) {
            strType = "update";
            if (utility.checkInternetConnection())
                getInquiryDetailApi();
        }
        spUnit.setOnItemSelectedListener(itemSelectListener);

        adapterYear = new HintAdapter(this, R.layout.simple_spinner_item, SplashActivity.arrYear) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                mTextView.setText(Html.fromHtml(SplashActivity.arrYear.get(position)));
                return mView;
            }
        };

        if (sessionUtil.isContain("iUserId")) {
            editEmail.setText(sessionUtil.getStringDetail("vEmailId"));
            editUserName.setText(sessionUtil.getStringDetail("vFirstName") + " " + sessionUtil.getStringDetail("vLastName"));
            editUserNumber.setText(sessionUtil.getStringDetail("vContactNumber").substring(0, 3) + "-" + sessionUtil.getStringDetail("vContactNumber").substring(3, 6) + "-" + sessionUtil.getStringDetail("vContactNumber").substring(6));
        }

        editUserNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length() - editUserNumber.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {

                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length() >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true;
                        //here is the core. we substring the raw digits and add the mask as convenient
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6);
                        editUserNumber.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        editUserNumber.setSelection(editUserNumber.getText().length() - cursorComplement);

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3);
                        editUserNumber.setText(ans);
                        editUserNumber.setSelection(editUserNumber.getText().length() - cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });

        editpContactName.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length() - editpContactName.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {

                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length() >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true;
                        //here is the core. we substring the raw digits and add the mask as convenient
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6);
                        editpContactName.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        editpContactName.setSelection(editpContactName.getText().length() - cursorComplement);

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3);
                        editpContactName.setText(ans);
                        editpContactName.setSelection(editpContactName.getText().length() - cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });

        editdContactNum.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length() - editdContactNum.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {

                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length() >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true;
                        //here is the core. we substring the raw digits and add the mask as convenient
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6);
                        editdContactNum.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        editdContactNum.setSelection(editdContactNum.getText().length() - cursorComplement);

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3);
                        editdContactNum.setText(ans);
                        editdContactNum.setSelection(editdContactNum.getText().length() - cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });
    }

    //methods

    public void finishActivity(View view) {
        finish();
    }

    private float getDistance() {
        Location locationA = new Location("point A");
        if (pLat != 0 || pLong != 0) {
            locationA.setLatitude(pLat);
            locationA.setLongitude(pLong);
        } else {
            return 0;
        }
        Location locationB = new Location("point B");
        if (dLat != 0 || dLong != 0) {
            locationB.setLatitude(dLat);
            locationB.setLongitude(dLong);
        } else {
            return 0;
        }
        return locationA.distanceTo(locationB) * 0.000621371f;
    }

    private void setUnitValues(int pos, int count) {
        View viewBox = View.inflate(PostInquiryActivity.this, R.layout.layout_vehicle_model, null);
        AutoCompleteTextView acVehicle = (AutoCompleteTextView) viewBox.findViewById(R.id.acVehicleModel);
        final TextView textYear = (TextView) viewBox.findViewById(R.id.textYear);
        EditText editVin = (EditText) viewBox.findViewById(R.id.editVin);

        acVehicle.setThreshold(1);
        acVehicle.setTypeface(Typeface.createFromAsset(getAssets(), "opensans_regular.ttf"));
        acVehicle.setAdapter(adapterModel);

        if (!listVehicleModel.isEmpty() && pos < listVehicleModel.size()) {
            acVehicle.setText(listVehicleModel.get(pos).modelName);
            editVin.setText(listVehicleModel.get(pos).VIN);
            textYear.setText(listVehicleModel.get(pos).modelYear);
        }

        textYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(PostInquiryActivity.this)
                        .setAdapter(adapterYear, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                textYear.setText(adapterYear.getItem(which));
                            }
                        }).create().show();
            }
        });

        if (llVehicleBox.getChildCount() < count) {
            llVehicleBox.addView(viewBox);
        } else if (llVehicleBox.getChildCount() > count) {
            llVehicleBox.removeViews(count, llVehicleBox.getChildCount() - count);
        }

        if (llVehicleBox.getChildCount() == 1) {
            utility.setHintColor(textYear);
            utility.setHintColor(acVehicle, acVehicle.getHint().toString());
        }
    }

    private void checkValidation() {
        View mView = llVehicleBox.getChildAt(0);
        TextView textYear = (TextView) mView.findViewById(R.id.textYear);
        AutoCompleteTextView acVehicleModel = (AutoCompleteTextView) mView.findViewById(R.id.acVehicleModel);

        if (!utility.isValidEmail(editEmail)) {
            editEmail.setError("Invalid Email Address.");
        } else if (!validateText(editpZipcode)) {
            editpZipcode.setError("Pickup Zip Code cannot be blank.");
        } else if (!validateText(editdZipcode)) {
            editdZipcode.setError("Delivery Zip Code cannot be blank.");
        } else if (textYear.getText().length() > 5) {
            utility.showOkDialog("Model Year cannot be black.");
        } else if (TextUtils.isEmpty(acVehicleModel.getText().toString())) {
            acVehicleModel.setError("Vehicle Model cannot be blank.");
            acVehicleModel.requestFocus();
        } else if (!validateText(editAnticipateDate)) {
            utility.showOkDialog("Anticipate Date cannot be blank.");
        } else {
            if (utility.checkInternetConnection())
                postInquiryApiCall();
        }
    }

    //api call

    private void getInquiryDetailApi() {
        stringHashMap.clear();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("iInquiryId", inquiryId);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    Gson gson = new Gson();
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        JSONObject objResult = jsonObject.getJSONObject("result");
                        JSONObject objMain = objResult.getJSONObject("main");
                        stringHashMap.clear();
                        stringHashMap.put("iInquiryId", objMain.getString("iInquiryId"));
                        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));

                        editEmail.setText(objMain.getString("vEmailId"));
                        editUserName.setText(objMain.getString("vUserName"));
                        editUserNumber.setText(objMain.getString("vUserContactNumber"));

                        editpContactName.setText(objMain.getString("vPickupContactName"));
                        editpContactNum.setText(objMain.getString("vPickupContactNumber"));
                        editpAddress.setText(objMain.getString("vPickupAddress"));
                        editpZipcode.setText(objMain.getString("vPickupZipCode"));

                        pLat = Double.parseDouble(objMain.getString("vPickupLatitude"));
                        pLong = Double.parseDouble(objMain.getString("vPickupLongitude"));

                        editdContactName.setText(objMain.getString("vDeliveryContactName"));
                        editdContactNum.setText(objMain.getString("vDeliveryContactNumber"));
                        editdAddress.setText(objMain.getString("vDeliveryAddress"));
                        editdZipcode.setText(objMain.getString("vDeliverZipCode"));

                        dLat = Double.parseDouble(objMain.getString("vDeliveryLatitude"));
                        dLong = Double.parseDouble(objMain.getString("vDeliveryLongitude"));

                        editdTotalDistance.setText(objMain.getString("fTotalDistance"));
                        spUnit.setSelection(adapterUnit.getPosition(objMain.getString("iUnit")));
                        editAnticipateDate.setText(objMain.getString("dAnticipatedDate"));
                        editInstruction.setText(objMain.getString("txSpecialInstruction"));

                        JSONArray arrVehicleDetail = objResult.getJSONArray("vehicleDetail");
                        llVehicleBox.removeAllViews();
                        for (int i = 0; i < arrVehicleDetail.length(); i++) {
                            VehicleDetailBean vehicleDetailBean = gson.fromJson(arrVehicleDetail.getJSONObject(i).toString(), VehicleDetailBean.class);
                            listVehicleModel.add(vehicleDetailBean);
                            setUnitValues(i, arrVehicleDetail.length());
                        }
                    } else {
                        Toast.makeText(PostInquiryActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, URL_INQUIRY_DETAILS);
    }

    private void postInquiryApiCall() {
        sbModelYear = new StringBuffer();
        sbModel = new StringBuffer();
        sbVin = new StringBuffer();

        for (int i = 0; i < llVehicleBox.getChildCount(); i++) {
            View mView = llVehicleBox.getChildAt(i);
            TextView textYear = (TextView) mView.findViewById(R.id.textYear);
            AutoCompleteTextView acVehicleModel = (AutoCompleteTextView) mView.findViewById(R.id.acVehicleModel);
            EditText editVin = (EditText) mView.findViewById(R.id.editVin);

            sbModelYear.append(textYear.getText().toString().trim().replace("*", ""));
            sbModelYear.append(",");

            sbModel.append(acVehicleModel.getText().toString());
            sbModel.append(",");

            sbVin.append(getTextStr(editVin));
            sbVin.append(",");
        }

        sbModelYear = sbModelYear.deleteCharAt(sbModelYear.lastIndexOf(","));
        sbModel = sbModel.deleteCharAt(sbModel.lastIndexOf(","));
        sbVin = sbVin.deleteCharAt(sbVin.lastIndexOf(","));

        if (sessionUtil.isContain("iUserId")) {
            stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        }
        stringHashMap.put("inquiryType", strType);
        stringHashMap.put("vEmailId", getTextStr(editEmail));
        stringHashMap.put("vUserName", getTextStr(editUserName));
        stringHashMap.put("vUserContactNumber", getTextStr(editUserNumber).replace("-", ""));

        stringHashMap.put("vPickupContactName", getTextStr(editpContactName));
        stringHashMap.put("vPickupContactNumber", getTextStr(editpContactNum).replace("-", ""));
        stringHashMap.put("vPickupAddress", getTextStr(editpAddress));
        stringHashMap.put("vPickupZipCode", getTextStr(editpZipcode));
        stringHashMap.put("vPickupLatitude", "" + pLat);
        stringHashMap.put("vPickupLongitude", "" + pLong);


        stringHashMap.put("vDeliveryContactName", getTextStr(editdContactName));
        stringHashMap.put("vDeliveryContactNumber", getTextStr(editdContactNum).replace("-", ""));
        stringHashMap.put("vDeliveryAddress", getTextStr(editdAddress));
        stringHashMap.put("vDeliverZipCode", getTextStr(editdZipcode));
        stringHashMap.put("vDeliveryLatitude", "" + dLat);
        stringHashMap.put("vDeliveryLongitude", "" + dLong);

        stringHashMap.put("fTotalDistance", getTextStr(editdTotalDistance));
        stringHashMap.put("iUnit", spUnit.getSelectedItem().toString());
        stringHashMap.put("iModelYearArr", sbModelYear.toString());
        stringHashMap.put("vModelNameArr", sbModel.toString());
        stringHashMap.put("vVINArr", sbVin.toString());

        stringHashMap.put("dAnticipatedDate", getTextStr(editAnticipateDate));
        stringHashMap.put("txSpecialInstruction", getTextStr(editInstruction));
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        finish();
                    }
                    Toast.makeText(PostInquiryActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_INQUIRY);
    }

    //listeners

    AdapterView.OnItemSelectedListener itemSelectListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (inquiryId != null) {
                llVehicleBox.removeAllViews();
            }
//            if (spUnit.getSelectedItemPosition() != arrUnit.size() - 1) {
//                btnSave.setEnabled(true);
//                for (int i = 0; i < position + 1; i++) {
//                    setUnitValues(i, Integer.parseInt(spUnit.getSelectedItem().toString()));
//                }
//            } else {
//                llVehicleBox.removeAllViews();
//                utility.showOkDialog("Please Contact ISY1");
//            }

            for (int i = 0; i < position + 1; i++) {
                setUnitValues(i, Integer.parseInt(spUnit.getSelectedItem().toString()));
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.YEAR, year);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        editAnticipateDate.setText(simpleDateFormat.format(cal.getTime()));
    }

    @Override
    public void onClick(View v) {
        utility.hideSoftKeyboard(PostInquiryActivity.this);
        if (v.equals(btnSave)) {
//            if (spUnit.getSelectedItemPosition() == arrUnit.size() - 1) {
//                utility.showOkDialog("Please Contact ISY1");
//            } else if (utility.checkInternetConnection())
//                checkValidation();

            checkValidation();

        } else if (v.equals(editAnticipateDate)) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(PostInquiryActivity.this, PostInquiryActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            datePickerDialog.show();
        }
    }

    TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (v.equals(editpContactNum)) {
                editpZipcode.requestFocus();
                return true;
            } else if (v.equals(editdContactNum)) {
                editdZipcode.requestFocus();
                return true;
            }
            return false;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 3) {
            editpAddress.setText(data.getStringExtra("location"));
            editpZipcode.setText(data.getStringExtra("zipcode"));
            pLat = data.getDoubleExtra("latitude", 0.0);
            pLong = data.getDoubleExtra("longitude", 0.0);

            stringHashMap.put("vPickupState", data.getStringExtra("statelocation"));
            stringHashMap.put("vPickupStateCode", data.getStringExtra("statecode"));
            stringHashMap.put("vPickupZipCode", data.getStringExtra("zipcode"));
            stringHashMap.put("vPickupCity", data.getStringExtra("city"));

            editdTotalDistance.setText("" + String.format("%.2f", getDistance()) + " Miles");
        } else if (requestCode == 2 && resultCode == 3) {

            editdAddress.setText(data.getStringExtra("location"));
            editdZipcode.setText(data.getStringExtra("zipcode"));
            dLat = data.getDoubleExtra("latitude", 0.0);
            dLong = data.getDoubleExtra("longitude", 0.0);

            stringHashMap.put("vDeliveryState", data.getStringExtra("statelocation"));
            stringHashMap.put("vDeliveryStateCode", data.getStringExtra("statecode"));
            stringHashMap.put("vDeliveryZipCode", data.getStringExtra("zipcode"));
            stringHashMap.put("vDeliveryCity", data.getStringExtra("city"));

            editdTotalDistance.setText("" + String.format("%.2f", getDistance()) + " Miles");
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        utility.hideSoftKeyboard(PostInquiryActivity.this);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (v.equals(editpAddress)) {
                Intent intent = new Intent(PostInquiryActivity.this, MapActivity.class);
                intent.putExtra("lat", pLat);
                intent.putExtra("long", pLong);
                intent.putExtra("hide", false);
                intent.putExtra("header", getResources().getString(R.string.hint_pickp_address));
                if (utility.checkInternetConnection())
                    startActivityForResult(intent, 1);
            } else if (v.equals(editdAddress) || v.equals(fetDeliver)) {
                Intent intent = new Intent(PostInquiryActivity.this, MapActivity.class);
                intent.putExtra("lat", dLat);
                intent.putExtra("long", dLong);
                intent.putExtra("hide", false);
                intent.putExtra("header", getResources().getString(R.string.lbl_delivery_address));
                if (utility.checkInternetConnection())
                    startActivityForResult(intent, 2);
            }
        }
        return true;
    }
}
