package com.isy1.inquiry;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.MapActivity;
import com.isy1.R;
import com.isy1.bean.VehicleDetailBean;
import com.isy1.policy.ContractAgreementActivity;
import com.isy1.util.AllAPICall;
import com.isy1.util.Constant;
import com.isy1.util.onAcceptListener;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class InquiryDetailsActivity extends BaseActivity implements View.OnClickListener {

    private TextView textHeader;
    private TextView textAccpet, textDecline;
    private TextView textEmail, textUserName, textContactNumber, textPContactName, textPContactNum, textPContactAdd,
            textDContactName, textDContactNum, textDContactAdd, textDistance, textUnit, textInquiryId,
            textAnticipateDate, textQuotePrice, textDate;
    private HashMap<String, String> stringHashMap;
    private LinearLayout llVehicleDetails, llFooter;
    private double pLat, pLong, dLat, dLong;
    private String inquiryId,vInquiryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry_details);
        initControl();
    }

    private void initControl() {
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText("Inquiry Details");

        textDate = (TextView) findViewById(R.id.textDate);
        textAnticipateDate = (TextView) findViewById(R.id.textAnticipateDate);
        textQuotePrice = (TextView) findViewById(R.id.textQuotePrice);
        textInquiryId = (TextView) findViewById(R.id.textInquiryId);
        textEmail = (TextView) findViewById(R.id.textEmail);
        textUserName = (TextView) findViewById(R.id.textUserName);
        textContactNumber = (TextView) findViewById(R.id.textContactNumber);
        textPContactName = (TextView) findViewById(R.id.textPContactName);
        textPContactNum = (TextView) findViewById(R.id.textPContactNum);
        textPContactAdd = (TextView) findViewById(R.id.textPContactAdd);
        textDContactName = (TextView) findViewById(R.id.textDContactName);
        textDContactNum = (TextView) findViewById(R.id.textDContactNum);
        textDContactAdd = (TextView) findViewById(R.id.textDContactAdd);
        textDistance = (TextView) findViewById(R.id.textDistance);
        textUnit = (TextView) findViewById(R.id.textUnit);

        textAccpet = (TextView) findViewById(R.id.textPay);
        textDecline = (TextView) findViewById(R.id.textReview);

        textAccpet.setText(R.string.lbl_accept);
        textDecline.setText(R.string.lbl_decline);

        textAccpet.setOnClickListener(this);
        textDecline.setOnClickListener(this);
        textPContactAdd.setOnClickListener(this);
        textDContactAdd.setOnClickListener(this);

        llVehicleDetails = (LinearLayout) findViewById(R.id.llVehicleDetails);

        if (utility.checkInternetConnection()) {
            getInquiryDetailApi();
        }
    }

    void getInquiryDetailApi() {
        stringHashMap = new HashMap<>();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("iInquiryId", getIntent().getStringExtra("inquiryId"));
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    Gson gson = new Gson();
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        JSONObject objResult = jsonObject.getJSONObject("result");
                        JSONObject objMain = objResult.getJSONObject("main");

                        textDate.setText(objMain.getString("iCreatedAt"));
                        inquiryId = objMain.getString("iInquiryId");
                        vInquiryId = objMain.getString("vInquiryNo");
                        textInquiryId.setText("Inquiry ID: " + vInquiryId);
                        utility.setTextColor(textInquiryId, textInquiryId.getText().toString(), ":", R.color.color_green);
                        textEmail.setText(objMain.getString("vEmailId"));
                        textUserName.setText(objMain.getString("vUserName"));
                        textContactNumber.setText(objMain.getString("vUserContactNumber"));
                        textPContactName.setText(objMain.getString("vPickupContactName"));
                        textPContactNum.setText(objMain.getString("vPickupContactNumber"));

                        StringBuffer stringBuffer = new StringBuffer();
                        if (!TextUtils.isEmpty(objMain.getString("vPickupAddress"))) {
                            stringBuffer.append(objMain.getString("vPickupAddress"));
                            stringBuffer.append(", ");
                        }
                        stringBuffer.append(objMain.getString("vPickupZipCode"));
                        textPContactAdd.setText(stringBuffer);

                        pLat = Double.parseDouble(objMain.getString("vPickupLatitude"));
                        pLong = Double.parseDouble(objMain.getString("vPickupLongitude"));

                        textDContactName.setText(objMain.getString("vDeliveryContactName"));
                        textDContactNum.setText(objMain.getString("vDeliveryContactNumber"));

                        StringBuffer stringBuffer1 = new StringBuffer();
                        if (!TextUtils.isEmpty(objMain.getString("vDeliveryAddress"))) {
                            stringBuffer1.append(objMain.getString("vDeliveryAddress"));
                            stringBuffer1.append(", ");
                        }
                        stringBuffer1.append(objMain.getString("vDeliverZipCode"));
                        textDContactAdd.setText(stringBuffer1);

                        dLat = Double.parseDouble(objMain.getString("vDeliveryLatitude"));
                        dLong = Double.parseDouble(objMain.getString("vDeliveryLongitude"));

                        textDistance.setText(objMain.getString("fTotalDistance") + " miles");
                        textUnit.setText(objMain.getString("iUnit"));
                        textAnticipateDate.setText("Anticipated Ship Date: " + objMain.getString("dAnticipatedDate"));
                        if (!objMain.getString("fQuote").equals("0") && !objMain.getString("fQuote").equals(""))
                            textQuotePrice.setVisibility(View.VISIBLE);
                        textQuotePrice.setText("Quote\n$" + objMain.getString("fQuote"));

                        JSONArray arrVehicleDetail = objResult.getJSONArray("vehicleDetail");
                        for (int i = 0; i < arrVehicleDetail.length(); i++) {
                            VehicleDetailBean vehicleDetailBean = gson.fromJson(arrVehicleDetail.getJSONObject(i).toString(), VehicleDetailBean.class);

                            View view = View.inflate(InquiryDetailsActivity.this, R.layout.row_vehicle_model, null);
                            TextView textVehicleYear = (TextView) view.findViewById(R.id.textVehicleYear);
                            TextView textVehicleModel = (TextView) view.findViewById(R.id.textVehicleModel);
                            TextView textVin = (TextView) view.findViewById(R.id.textVin);

                            textVehicleYear.setText(vehicleDetailBean.modelYear);
                            textVehicleModel.setText(vehicleDetailBean.modelName);
                            textVin.setText(vehicleDetailBean.VIN);

                            llVehicleDetails.addView(view);
                        }
                        if (objMain.getString("eIsApproved").equalsIgnoreCase("n")) {
                            textAccpet.setEnabled(false);
                        }

                        if (objMain.getString("eIsCanceled").equalsIgnoreCase("y")) {
                            llFooter = (LinearLayout) findViewById(R.id.llFooter);
                            llFooter.setVisibility(View.GONE);
                        }

                    } else {
                        Toast.makeText(InquiryDetailsActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_INQUIRY_DETAILS);
    }

    public void finishActivity(View view) {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textAccpet)) {
            Intent intent = new Intent(this, ContractAgreementActivity.class);
            intent.putExtra("iInquiryId", inquiryId);
            startActivityForResult(intent, 0);
        } else if (v.equals(textDecline)) {
            utility.showConfirmationDialog("Are you sure you want to decline inquiry ?", new onAcceptListener() {
                @Override
                public void onAccept(boolean isTrue) {
                    cancelInquiry(inquiryId);
                }
            });
        } else if (v.equals(textPContactAdd)) {
            Intent intent = new Intent(this, MapActivity.class);
            intent.putExtra("lat", pLat);
            intent.putExtra("long", pLong);
            intent.putExtra("header", "Pickup Location");
            intent.putExtra("hide", true);
            startActivity(intent);

        } else if (v.equals(textDContactAdd)) {
            Intent intent = new Intent(this, MapActivity.class);
            intent.putExtra("lat", dLat);
            intent.putExtra("long", dLong);
            intent.putExtra("header", "Delivery Location");
            intent.putExtra("hide", true);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
        setResult(1);
    }

    private void cancelInquiry(String inquiryId) {
        stringHashMap.clear();

        stringHashMap.put("iInquiryId", inquiryId);
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("inquiryType", "cancel");

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        finish();
                    }
                    Toast.makeText(InquiryDetailsActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_INQUIRY);
    }
}
