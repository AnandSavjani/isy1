package com.isy1.inquiry;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.isy1.R;
import com.isy1.adapter.InquiryAdapter;
import com.isy1.bean.InquiryBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.Constant;
import com.isy1.util.SessionUtil;
import com.isy1.util.Utility;
import com.isy1.util.onAcceptListener;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InquiryFragment extends Fragment implements AbsListView.OnScrollListener {

    int position;
    private SwipeMenuListView lvInquiry;
    private InquiryAdapter inquiryAdapter;
    private List<InquiryBean> listInquiry;
    private RadioGroup rgFilter;
    private Utility utility;
    private HashMap<String, String> stringHashMap;
    private SessionUtil sessionUtil;
    private String filterType = "all";
    private TextView textNoResult;
    int nextPos, flag = 0;
    private String searchString = "";
    private RadioButton rbAll, rbWithQuote, rbWithoutQuote;

    public static InquiryFragment newInstance(int position) {
        InquiryFragment f = new InquiryFragment();
        Bundle b = new Bundle();
        b.putInt("position", position);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_main, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControl(view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("position");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            listInquiry.clear();
            nextPos = 0;
            getAllQuery(0, searchString);
        }
        rgFilter.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    private void initControl(View view) {
        listInquiry = new ArrayList<>();
        stringHashMap = new HashMap<>();

        sessionUtil = new SessionUtil(getActivity());
        utility = new Utility(getActivity());

        textNoResult = (TextView) view.findViewById(R.id.textNoResult);

        rgFilter = (RadioGroup) view.findViewById(R.id.rgFilter);

        rbAll = (RadioButton) view.findViewById(R.id.rbAll);
        rbWithQuote = (RadioButton) view.findViewById(R.id.rbWithQuote);
        rbWithoutQuote = (RadioButton) view.findViewById(R.id.rbWithoutQuote);

        lvInquiry = (SwipeMenuListView) view.findViewById(R.id.lvInquiry);
        lvInquiry.setMenuCreator(menuCreator);
        lvInquiry.setOnItemClickListener(onItemClickListener);
        lvInquiry.setOnMenuItemClickListener(onMenuItemClickListener);
        lvInquiry.setOnScrollListener(this);

        inquiryAdapter = new InquiryAdapter(getActivity(), listInquiry);
        lvInquiry.setAdapter(inquiryAdapter);
    }

    public void searchForInquiry(String query) {
        searchString = query;
        onResume();
    }

    private void getAllQuery(final int start, String search) {
        stringHashMap.clear();

        stringHashMap.put("inquiryType", "view");
        stringHashMap.put("filterType", filterType);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("pageStart", "" + start);
        stringHashMap.put("search", search);

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Gson gson = new Gson();
                    if (jsonObject.getInt("status") == 1) {
                        flag = 0;
                        nextPos = nextPos + 10;
                        JSONArray arrResult = jsonObject.getJSONArray("result");
                        for (int i = 0; i < arrResult.length(); i++) {
                            InquiryBean inquiryBean = gson.fromJson(arrResult.getJSONObject(i).toString(), InquiryBean.class);
                            inquiryBean.label = "Inquiry ID";
                            listInquiry.add(inquiryBean);
                        }
                        lvInquiry.setVisibility(View.VISIBLE);
                        textNoResult.setVisibility(View.GONE);

                    } else if (jsonObject.getInt("status") == 0) {
                        flag = -1;
                        if (listInquiry.isEmpty()) {
                            textNoResult.setVisibility(View.VISIBLE);
                            lvInquiry.setVisibility(View.GONE);
                        }
                    }
                    inquiryAdapter.notifyDataSetChanged();
//                    lvInquiry.setSelectionFromTop(start - 1, 0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_INQUIRY);
    }

    private void cancelInquiry(String inquiryId, final int listPos) {
        stringHashMap.clear();

        stringHashMap.put("iInquiryId", inquiryId);
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("inquiryType", "cancel");

        new AllAPICall(getActivity(), stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        listInquiry.remove(listPos);
                        inquiryAdapter.notifyDataSetChanged();
                    }
                    Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constant.URL_INQUIRY);
    }

    //*******listener

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_IDLE && lvInquiry.getLastVisiblePosition() >= nextPos - 1 && flag != -1) {
            if (utility.checkInternetConnection()) {
                getAllQuery(nextPos, searchString);
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (utility.checkInternetConnection())
                startActivity(new Intent(getActivity(), InquiryDetailsActivity.class).putExtra("inquiryId", listInquiry.get(position).inquiryId));
        }
    };

    RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        private boolean isChecked(RadioGroup group, int viewId) {
            if (viewId != -1) {
                View v = group.findViewById(viewId);
                if (v instanceof RadioButton) {
                    return ((RadioButton) v).isChecked();
                }
            }
            return true;
        }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton radioButton;
            if (!isChecked(group, checkedId)) {
                return;
            }
            radioButton = (RadioButton) group.findViewById(group.getCheckedRadioButtonId());
            filterType = radioButton.getText().toString().toLowerCase().replace(" ", "");
            if (utility.checkInternetConnection()) {
                listInquiry.clear();
                nextPos = 0;
                getAllQuery(0, searchString);
            }
        }
    };

    SwipeMenuListView.OnMenuItemClickListener onMenuItemClickListener = new SwipeMenuListView.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
            switch (index) {
                case 0:
                    if (!listInquiry.get(position).quotedPrice.equals("")) {
                        utility.showOkDialog("Your Inquiry is in processing...");
                    } else {
                        if (utility.checkInternetConnection())
                            startActivity(new Intent(getActivity(), PostInquiryActivity.class).putExtra("header", getResources().getString(R.string.lbl_edit_inquiry)).putExtra("inquiryId", listInquiry.get(position).inquiryId));
                    }

                    break;
                case 1:
                    if (listInquiry.get(position).quotedPrice.equals("")) {
                        utility.showConfirmationDialog("Are you sure you want to cancel the Inquiry?", new onAcceptListener() {
                            @Override
                            public void onAccept(boolean isTrue) {
                                if (utility.checkInternetConnection())
                                    cancelInquiry(listInquiry.get(position).inquiryId, position);
                            }
                        });
                    } else {
                        utility.showOkDialog("Your Inquiry is in processing...");
                    }
                    break;
            }
            return false;
        }
    };

    SwipeMenuCreator menuCreator = new SwipeMenuCreator() {
        @Override
        public void create(SwipeMenu menu) {
            // create "edit" item
            SwipeMenuItem openItem = new SwipeMenuItem(getActivity());
            openItem.setWidth(utility.dp2px(90));
            openItem.setIcon(R.drawable.edit_sw);
            menu.addMenuItem(openItem);

            // create "cancel" item
            SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
            deleteItem.setWidth(utility.dp2px(90));
            deleteItem.setIcon(R.drawable.cancle);
            menu.addMenuItem(deleteItem);
        }
    };
}

