package com.isy1.inquiry;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.MapActivity;
import com.isy1.R;
import com.isy1.bean.VehicleDetailBean;
import com.isy1.notification.MessagesActivity;
import com.isy1.setting.FeedbackActivity;
import com.isy1.util.AllAPICall;
import com.isy1.util.onAcceptListener;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class OrderDetailsActivity extends BaseActivity implements View.OnClickListener {

    private TextView textHeader;
    private ImageView imgStatusPin, imgStatus1, imgStatus2, imgStatus3, imgStatus4, imgStatus5, imgStatus6;
    private TextView textCarrierName, textCarrierDate, textCarrierContactNo, textPickupDate, textDeliveryDate,
            textInvoiceDate, textRecieveDate, textDate, textQuotePrice, textInquiryId, textAnticipateDate, textEmail, textUserName,
            textContactNumber, textPContactName, textPContactNum, textPContactAdd, textDContactName, textDContactNum, textDContactAdd,
            textDistance, textUnit, textStatus, textPay, textReview;
    private LinearLayout llVehicleDetails, llFooter;
    int pos, statusCode = 0;
    String header = "", url;
    private HashMap<String, String> stringHashMap;
    private double pLat, pLong, dLat, dLong;
    String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        initControl();
    }

    private void initControl() {
        stringHashMap = new HashMap<>();

        pos = getIntent().getIntExtra("position", 0);
        header = getIntent().getStringExtra("header");
        from = getIntent().getStringExtra("from");

        llFooter = (LinearLayout) findViewById(R.id.llFooter);
        if (from.equals("history")) {
            llFooter.setVisibility(View.GONE);
        }

        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(header);

        imgStatusPin = (ImageView) findViewById(R.id.imgStatusPin);
        imgStatus1 = (ImageView) findViewById(R.id.imgStatus1);
        imgStatus2 = (ImageView) findViewById(R.id.imgStatus2);
        imgStatus3 = (ImageView) findViewById(R.id.imgStatus3);
        imgStatus4 = (ImageView) findViewById(R.id.imgStatus4);
        imgStatus5 = (ImageView) findViewById(R.id.imgStatus5);
        imgStatus6 = (ImageView) findViewById(R.id.imgStatus6);

        textCarrierName = (TextView) findViewById(R.id.textCarrierName);
        textCarrierDate = (TextView) findViewById(R.id.textCarrierDate);
        textCarrierContactNo = (TextView) findViewById(R.id.textCarrierContactNo);
        textPickupDate = (TextView) findViewById(R.id.textPickupDate);
        textDeliveryDate = (TextView) findViewById(R.id.textDeliveryDate);
        textInvoiceDate = (TextView) findViewById(R.id.textInvoiceDate);
        textRecieveDate = (TextView) findViewById(R.id.textRecieveDate);
        textDate = (TextView) findViewById(R.id.textDate);
        textQuotePrice = (TextView) findViewById(R.id.textQuotePrice);
        textInquiryId = (TextView) findViewById(R.id.textInquiryId);
        textAnticipateDate = (TextView) findViewById(R.id.textAnticipateDate);
        textEmail = (TextView) findViewById(R.id.textEmail);
        textUserName = (TextView) findViewById(R.id.textUserName);
        textContactNumber = (TextView) findViewById(R.id.textContactNumber);
        textPContactName = (TextView) findViewById(R.id.textPContactName);
        textPContactNum = (TextView) findViewById(R.id.textPContactNum);
        textPContactAdd = (TextView) findViewById(R.id.textPContactAdd);
        textDContactName = (TextView) findViewById(R.id.textDContactName);
        textDContactNum = (TextView) findViewById(R.id.textDContactNum);
        textDContactAdd = (TextView) findViewById(R.id.textDContactAdd);
        textDistance = (TextView) findViewById(R.id.textDistance);
        textUnit = (TextView) findViewById(R.id.textUnit);
        textStatus = (TextView) findViewById(R.id.textStatus);

        textPay = (TextView) findViewById(R.id.textPay);
        textReview = (TextView) findViewById(R.id.textReview);

        textPay.setOnClickListener(this);
        textReview.setOnClickListener(this);

        llVehicleDetails = (LinearLayout) findViewById(R.id.llVehicleDetails);

        if (pos == 2) {
            textReview.setVisibility(View.GONE);
            textPay.setText(R.string.lbl_feedback);
            url = URL_DELIVERED_DETAIL;
        } else if (pos == 4) {
            textReview.setVisibility(View.GONE);
            textPay.setText(R.string.lbl_make_payment);
            textPay.setEnabled(false);
            url = URL_INVOICE_DETAIL;
        } else {
            if (header.equalsIgnoreCase("Inquiry Details")) {
                textPay.setText(R.string.lbl_accept);
                textReview.setText(R.string.lbl_decline);
            } else if (header.equalsIgnoreCase("Order Details")) {
                textPay.setText(R.string.lbl_message);
                textReview.setText(R.string.btn_cancel_order);
                url = URL_ORDER_DETAILS;
            }
        }
        textPContactAdd.setOnClickListener(this);
        textDContactAdd.setOnClickListener(this);

        if (utility.checkInternetConnection())
            orderDetailApiCall();
    }

    public void finishActivity(View view) {
        finish();
    }

    private void orderDetailApiCall() {
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("iOrderId", getIntent().getStringExtra("iOrderId"));
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        JSONObject objResult = jsonObject.getJSONObject("result");
                        JSONObject objMain = objResult.getJSONObject("main");

                        textInquiryId.setText("Order ID: " + objMain.getString("vOrderNo"));
                        utility.setTextColor(textInquiryId, textInquiryId.getText().toString(), ":", R.color.color_green);
                        textDate.setText(objMain.getString("iCreatedAt"));
                        statusCode = objMain.getInt("iOrderStatusCode");

                        textStatus.setText("Status: " + objMain.getString("vOrderStatus"));
                        utility.setTextColor(textStatus, textStatus.getText().toString(), ":", R.color.color_green);

                        textCarrierName.setText(objMain.getString("vCompanyName"));
                        textCarrierContactNo.setText(objMain.getString("vMobileNumber"));
                        textCarrierDate.setText(objMain.getString("iCarrierAssignedDate"));

                        if (objMain.getString("iCarrierAssignedDate").equals("")) {
                            imgStatus2.setImageResource(R.drawable.status_g_2);
                        }

                        textPickupDate.setText(objMain.getString("iPickupETD"));
                        if (objMain.getString("iPickupETD").equals("")) {
                            imgStatus3.setImageResource(R.drawable.status_g_3);
                        }

                        textDeliveryDate.setText(objMain.getString("iDeliveryETA"));
                        if (objMain.getString("iDeliveryETA").equals("")) {
                            imgStatus4.setImageResource(R.drawable.status_g_4);
                        }

                        textInvoiceDate.setText(objMain.getString("iInvoiceSentDate"));
                        if (objMain.getString("iInvoiceSentDate").equals("")) {
                            imgStatus5.setImageResource(R.drawable.status_g_5);
                        }

                        textRecieveDate.setText(objMain.getString("iPaymentRecivedDate"));
                        if (objMain.getString("iPaymentRecivedDate").equals("")) {
                            imgStatus6.setImageResource(R.drawable.status_g_6);
                        }
                        textEmail.setText(objMain.getString("vEmailId"));
                        textUserName.setText(objMain.getString("vUserName"));
                        textContactNumber.setText(objMain.getString("vUserContactNumber"));

                        textPContactName.setText(objMain.getString("vPickupContactName"));
                        textPContactNum.setText(objMain.getString("vPickupContactNumber"));
                        textPContactAdd.setText(objMain.getString("vPickupAddress") + ", " + objMain.getString("vPickupZipCode"));
                        pLat = objMain.getDouble("vPickupLatitude");
                        pLong = objMain.getDouble("vPickupLongitude");

                        textDContactName.setText(objMain.getString("vDeliveryContactName"));
                        textDContactNum.setText(objMain.getString("vDeliveryContactNumber"));
                        textDContactAdd.setText(objMain.getString("vDeliveryAddress") + ", " + objMain.getString("vDeliverZipCode"));
                        dLat = objMain.getDouble("vDeliveryLatitude");
                        dLong = objMain.getDouble("vDeliveryLongitude");

                        textDistance.setText(objMain.getString("fTotalDistance"));

                        textUnit.setText("" + objMain.getInt("iUnit"));
                        textAnticipateDate.setText("Anticipated Ship Date: " + objMain.getString("dAnticipatedDate"));
                        if (!objMain.getString("fQuote").equals("")) {
                            textQuotePrice.setVisibility(View.VISIBLE);
                            textQuotePrice.setText("Quote\n $" + objMain.getString("fQuote"));
                        }

                        Gson gson = new Gson();
                        JSONArray arrVehicleDetail = objResult.getJSONArray("vehicleDetail");
                        for (int i = 0; i < arrVehicleDetail.length(); i++) {
                            VehicleDetailBean vehicleDetailBean = gson.fromJson(arrVehicleDetail.getJSONObject(i).toString(), VehicleDetailBean.class);

                            View view = View.inflate(OrderDetailsActivity.this, R.layout.row_vehicle_model, null);
                            TextView textVehicleYear = (TextView) view.findViewById(R.id.textVehicleYear);
                            TextView textVehicleModel = (TextView) view.findViewById(R.id.textVehicleModel);
                            TextView textVin = (TextView) view.findViewById(R.id.textVin);

                            textVehicleYear.setText(vehicleDetailBean.modelYear);
                            textVehicleModel.setText(vehicleDetailBean.modelName);
                            textVin.setText(vehicleDetailBean.VIN);

                            llVehicleDetails.addView(view);
                        }

                        Drawable pin = getResources().getDrawable(getResources()
                                .getIdentifier("status_pin_" + statusCode, "drawable", getPackageName()));

                        imgStatusPin.setImageDrawable(pin);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setResult(1);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textPay)) {
            if (pos != 2 && pos != 4) {
                if (header.equalsIgnoreCase("Inquiry Details")) {
                    utility.showConfirmationDialog("Are you sure you want to accept the Inquiry ?", new onAcceptListener() {
                        @Override
                        public void onAccept(boolean isTrue) {

                        }
                    });
                } else if (header.equalsIgnoreCase("Order Details")) {
                    Intent intent = new Intent(this, MessagesActivity.class);
                    intent.putExtra("messageType", "order");
                    startActivity(intent);
                }
            } else if (pos == 2) {
                startActivity(new Intent(OrderDetailsActivity.this, FeedbackActivity.class)
                        .putExtra("iOrderId", getIntent().getStringExtra("iOrderId")));
            } else {
                utility.showConfirmationDialog("Are you sure you want to accept the Order ?", new onAcceptListener() {
                    @Override
                    public void onAccept(boolean isTrue) {
                        Toast.makeText(OrderDetailsActivity.this, "baki che", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }

        } else if (v.equals(textReview)) {
            if (statusCode < 3) {
                utility.showConfirmationDialog("Are you sure you want to cancel the Order ?", new onAcceptListener() {
                    @Override
                    public void onAccept(boolean isTrue) {
                        if (utility.checkInternetConnection()) {
                            cancelOrder();
                        }
                    }
                });
            } else {
                utility.showOkDialog("Your order is in processing...");
            }
        } else if (v.equals(textPContactAdd)) {
            Intent intent = new Intent(this, MapActivity.class);
            intent.putExtra("lat", pLat);
            intent.putExtra("long", pLong);
            intent.putExtra("header", "Pickup Location");
            intent.putExtra("hide", true);
            startActivity(intent);
        } else if (v.equals(textDContactAdd)) {
            Intent intent = new Intent(this, MapActivity.class);
            intent.putExtra("lat", dLat);
            intent.putExtra("long", dLong);
            intent.putExtra("header", "Delivery Location");
            intent.putExtra("hide", true);
            startActivity(intent);
        }
    }

    private void cancelOrder() {
        stringHashMap.clear();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("orderType", "cancel");
        stringHashMap.put("iOrderId", getIntent().getStringExtra("iOrderId"));
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        finish();
                    }
                    Toast.makeText(OrderDetailsActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_ORDERS);
    }
}
