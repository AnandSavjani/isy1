package com.isy1.policy;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.inquiry.OrderDetailsActivity;
import com.isy1.util.AllAPICall;
import com.isy1.util.onAcceptListener;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

public class ContractAgreementActivity extends BaseActivity implements View.OnClickListener {

    private TextView textAgreement, textClear, textDigitalSign;
    private TextView textAccept, textDecline;
    private HashMap<String, String> stringHashMap;
    private SignaturePad signPad;
    boolean isSigned = false;
    private TextView textHeader;
    private HashMap<String, File> fileHashMap;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement);
        initControl();
    }

    private void initControl() {
        stringHashMap = new HashMap<>();
        fileHashMap = new HashMap<>();

        textAgreement = (TextView) findViewById(R.id.textAgreement);
        textClear = (TextView) findViewById(R.id.textClear);
        textDigitalSign = (TextView) findViewById(R.id.textDigitalSign);
        textHeader = (TextView) findViewById(R.id.textHeader);

        utility.setHintColor(textDigitalSign);

        textAccept = (TextView) findViewById(R.id.textPay);
        textDecline = (TextView) findViewById(R.id.textReview);
        textAccept.setText(R.string.lbl_accept);
        textDecline.setText(R.string.lbl_decline);

        signPad = (SignaturePad) findViewById(R.id.signPad);
        signPad.setOnSignedListener(onSignedListener);

        textClear.setOnClickListener(this);
        textAccept.setOnClickListener(this);
        textDecline.setOnClickListener(this);

        stringHashMap.put("iPageId", "1");
        if (utility.checkInternetConnection()) {
            new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getInt("status") == 1) {
                            JSONObject objresult = jsonObject.getJSONObject("result");
                            textAgreement.setText(Html.fromHtml(objresult.getString("txContent")));
                            textHeader.setText(objresult.getString("vPageName"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_PAGES);
        }
    }

    public void finishActivity(View view) {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textClear)) {
            signPad.clear();
            isSigned = false;
        } else if (v.equals(textAccept)) {
            if (!isSigned) {
                utility.showOkDialog("Please First Sign!");
                return;
            }
            String path = utility.addSignatureToGallery(signPad.getSignatureBitmap());
            if (path != null) {
                file = new File(path);
            } else {
                Toast.makeText(ContractAgreementActivity.this, "Unable to store the signature", Toast.LENGTH_SHORT).show();
            }

            utility.showConfirmationDialog("Are you sure you want to accept agreement ?", new onAcceptListener() {
                @Override
                public void onAccept(boolean isTrue) {
                    if (utility.checkInternetConnection())
                        acceptInquiryApiCall();

                }
            });
        } else if (v.equals(textDecline)) {
            utility.showConfirmationDialog("Are you sure you want to decline agreement ?", new onAcceptListener() {
                @Override
                public void onAccept(boolean isTrue) {
                    finish();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setResult(1);
        finish();
    }

    SignaturePad.OnSignedListener onSignedListener = new SignaturePad.OnSignedListener() {
        @Override
        public void onSigned() {
            isSigned = true;
        }

        @Override
        public void onClear() {
            isSigned = false;
        }
    };

    private void acceptInquiryApiCall() {
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("iInquiryId", getIntent().getStringExtra("iInquiryId"));

        fileHashMap.put("vAgreementPic", file);

        new AllAPICall(this, stringHashMap, fileHashMap, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        Intent intent = new Intent(ContractAgreementActivity.this, OrderDetailsActivity.class);
                        intent.putExtra("header", "Order Details");
                        intent.putExtra("iOrderId", jsonObject.getJSONObject("result").getString("iOrderId"));
                        intent.putExtra("from","");
                        startActivityForResult(intent, 0);
                        finish();
                    } else {
                        Toast.makeText(ContractAgreementActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_CONTRACT_AGREEMENT);
    }
}

