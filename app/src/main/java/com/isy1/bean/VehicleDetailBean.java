package com.isy1.bean;


import com.google.gson.annotations.SerializedName;

public class VehicleDetailBean {

    @SerializedName("iModelYear")
    public String modelYear;

    @SerializedName("vModelName")
    public String modelName;

    @SerializedName("vVIN")
    public String VIN;
}
