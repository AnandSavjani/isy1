package com.isy1.bean;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InquiryBean implements Serializable {

    public String label;
    public String vehicleModel;

    @SerializedName("vInquiryNo")
    public String inquiryNo;
    @SerializedName("dAnticipatedDate")
    public String date;
    @SerializedName("vDeliveryAddress")
    public String deliveryAddress;
    @SerializedName("fQuote")
    public String quotedPrice;
    @SerializedName("vPickupAddress")
    public String pickupAddress;
    @SerializedName("iInquiryId")
    public String inquiryId;
    @SerializedName("vUserName")
    public String userName;
    @SerializedName("iCreatedAt")
    public String createdAt;


}
