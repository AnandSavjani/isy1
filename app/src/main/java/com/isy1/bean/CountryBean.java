package com.isy1.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CountryBean implements Serializable{

    @SerializedName("vCountryName")
    public String countryName;
    @SerializedName("vCountryCode")
    public String countryCode;

}
