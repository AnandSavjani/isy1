package com.isy1.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyHistoryBean implements Serializable {

    public String label;

    @SerializedName("iOrderId")
    public String orderId;

    @SerializedName("vOrderNo")
    public String orderNo;

    @SerializedName("vUserName")
    public String userName;

    @SerializedName("vehicleModel")
    public String vehicleModel;

    @SerializedName("vPickupAddress")
    public String pickupAddress;

    @SerializedName("vDeliveryAddress")
    public String deliveryAddress;

    @SerializedName("dAnticipatedDate")
    public String anticipatedDate;

    @SerializedName("fQuote")
    public String quote;

}
