package com.isy1.bean;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationBean implements Serializable {

    @SerializedName("iCreatedAt")
    public String createdAt;
    @SerializedName("iNotificationId")
    public String notificationId;
    @SerializedName("vMessageTitle")
    public String messageTitle;
    @SerializedName("txMessage")
    public String message;
    @SerializedName("vNotificationType")
    public String notificationType;
    @SerializedName("iInquiryId")
    public String inquiryId;
    @SerializedName("iOrderId")
    public String orderId;

}
