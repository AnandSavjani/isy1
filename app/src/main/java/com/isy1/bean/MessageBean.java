package com.isy1.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MessageBean implements Serializable {

    @SerializedName("iCreatedAt")
    public String createdAt;
    @SerializedName("messageHead")
    public String messageHead;
    @SerializedName("txMessage")
    public String txMessage;
}
