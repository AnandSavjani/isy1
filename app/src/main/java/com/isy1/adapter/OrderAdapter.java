package com.isy1.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.isy1.R;
import com.isy1.bean.OrderBean;
import com.isy1.util.Utility;
import com.isy1.util.ViewHolder;

import java.util.List;

public class OrderAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private Utility utility;
    private List<OrderBean> listOrder;

    public OrderAdapter(Context context, List<OrderBean> listOrder) {
        this.context = context;
        utility = new Utility(context);
        this.listOrder = listOrder;
    }

    @Override
    public int getCount() {
        return listOrder.size();
    }

    @Override
    public OrderBean getItem(int position) {
        return listOrder.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.row_orders, null);
            viewHolder.textDate = (TextView) convertView.findViewById(R.id.textDate);
            viewHolder.textOrderNo = (TextView) convertView.findViewById(R.id.textOrderNo);
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textName);
            viewHolder.textVehicleModel = (TextView) convertView.findViewById(R.id.textVehicleModel);
            viewHolder.textPickupAdd = (TextView) convertView.findViewById(R.id.textPickupAdd);
            viewHolder.textDeliveryAdd = (TextView) convertView.findViewById(R.id.textDeliveryAdd);
            viewHolder.textStatus = (TextView) convertView.findViewById(R.id.textStatus);
            viewHolder.imgStatus = (ImageView) convertView.findViewById(R.id.imgStatus);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textDate.setText(getItem(position).createdAt);
        viewHolder.textOrderNo.setText(getItem(position).orderNo);
        viewHolder.textName.setText(getItem(position).userName);
        viewHolder.textVehicleModel.setText(getItem(position).vehicleModel);
        viewHolder.textPickupAdd.setText(getItem(position).pickupAddress);
        viewHolder.textDeliveryAdd.setText(getItem(position).deliveryAddress);
        viewHolder.textStatus.setText("Status - " + getItem(position).orderStatus);

        Drawable status = ContextCompat.getDrawable(context, context.getResources()
                .getIdentifier("status_pin_" + getItem(position).orderStatusCode, "drawable", context.getPackageName()));
        viewHolder.imgStatus.setImageDrawable(status);
        utility.setTextColor(viewHolder.textStatus, viewHolder.textStatus.getText().toString(), "-", R.color.color_green);

        return convertView;
    }
}