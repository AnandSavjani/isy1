package com.isy1.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.isy1.R;
import com.isy1.bean.NotificationBean;
import com.isy1.util.Utility;
import com.isy1.util.ViewHolder;

import java.util.List;

public class NotificationAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private Utility utility;
    private List<NotificationBean> listNotification;

    public NotificationAdapter(Context context, List<NotificationBean> listNotification) {
        this.context = context;
        utility = new Utility(context);
        this.listNotification = listNotification;
    }

    @Override
    public int getCount() {
        return listNotification.size();
    }

    @Override
    public NotificationBean getItem(int position) {
        return listNotification.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_notification, null);
            viewHolder.textDate = (TextView) convertView.findViewById(R.id.textDate);
            viewHolder.textOrderNo = (TextView) convertView.findViewById(R.id.textOrderNo);
            viewHolder.textNotifications = (TextView) convertView.findViewById(R.id.textNotifications);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textDate.setText(getItem(position).createdAt);
        viewHolder.textOrderNo.setText(getItem(position).messageTitle);
        viewHolder.textNotifications.setText(getItem(position).message);

        utility.setTextColor(viewHolder.textOrderNo, viewHolder.textOrderNo.getText().toString(), ":", R.color.color_green);
        return convertView;
    }
}
