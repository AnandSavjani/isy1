package com.isy1.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.isy1.R;
import com.isy1.bean.OrderBean;
import com.isy1.inquiry.OrderDetailsActivity;
import com.isy1.util.Utility;
import com.isy1.util.ViewHolder;

import java.util.List;


public class MyInvoiceAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private Utility utility;
    private List<OrderBean> listInvoice;

    public MyInvoiceAdapter(Context context, List<OrderBean> listInvoice) {
        this.context = context;
        utility = new Utility(context);
        this.listInvoice = listInvoice;
    }

    @Override
    public int getCount() {
        return listInvoice.size();
    }

    @Override
    public OrderBean getItem(int position) {
        return listInvoice.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_my_invoice, null);
            viewHolder.textDate = (TextView) convertView.findViewById(R.id.textDate);
            viewHolder.textDeliveryAdd = (TextView) convertView.findViewById(R.id.textDeliveryAdd);
            viewHolder.textInquiryNo = (TextView) convertView.findViewById(R.id.textOrderNo);
            viewHolder.textPickupAdd = (TextView) convertView.findViewById(R.id.textPickupAdd);
            viewHolder.textVehicleModel = (TextView) convertView.findViewById(R.id.textVehicleModel);
            viewHolder.textViewInvoice = (TextView) convertView.findViewById(R.id.textViewInvoice);
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textName);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, OrderDetailsActivity.class)
                        .putExtra("position", 4)
                        .putExtra("iOrderId", getItem(position).orderId)
                        .putExtra("header", context.getResources().getString(R.string.lbl_my_invoice))
                        .putExtra("from", "")
                );
            }
        });

        viewHolder.textDate.setText(getItem(position).createdAt);
        viewHolder.textDeliveryAdd.setText(getItem(position).deliveryAddress);
        viewHolder.textInquiryNo.setText(getItem(position).orderNo);
        viewHolder.textPickupAdd.setText(getItem(position).pickupAddress);
        viewHolder.textVehicleModel.setText(getItem(position).vehicleModel);
        viewHolder.textName.setText(getItem(position).userName);

        return convertView;
    }
}
