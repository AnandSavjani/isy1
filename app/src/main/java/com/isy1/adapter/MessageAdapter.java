package com.isy1.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.isy1.R;
import com.isy1.bean.MessageBean;
import com.isy1.util.ViewHolder;

import java.util.List;


public class MessageAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    List<MessageBean> listMessage;

    public MessageAdapter(Context context, List<MessageBean> listMessage) {
        this.context = context;
        this.listMessage = listMessage;
    }

    @Override
    public int getCount() {
        return listMessage.size();
    }

    @Override
    public MessageBean getItem(int position) {
        return listMessage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_message, null);

            viewHolder.textNotifications = (TextView) convertView.findViewById(R.id.textNotifications);
            viewHolder.textOrderNo = (TextView) convertView.findViewById(R.id.textOrderNo);
            viewHolder.textDate = (TextView) convertView.findViewById(R.id.textDate);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textNotifications.setText(getItem(position).txMessage);
        viewHolder.textOrderNo.setText(getItem(position).messageHead);
        viewHolder.textDate.setText(getItem(position).createdAt);

        return convertView;
    }
}
