package com.isy1.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.isy1.R;
import com.isy1.bean.MyHistoryBean;
import com.isy1.util.Utility;
import com.isy1.util.ViewHolder;

import java.util.List;


public class MyHistoryAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder viewHolder;
    private Utility utility;
    private List<MyHistoryBean> listInquiry;

    public MyHistoryAdapter(Context context, List<MyHistoryBean> listInquiry) {
        this.context = context;
        utility = new Utility(context);
        this.listInquiry = listInquiry;
    }

    @Override
    public int getCount() {
        return listInquiry.size();
    }

    @Override
    public MyHistoryBean getItem(int position) {
        return listInquiry.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.row_inquiry, null);
            viewHolder.textDate = (TextView) convertView.findViewById(R.id.textDate);
            viewHolder.textDeliveryAdd = (TextView) convertView.findViewById(R.id.textDeliveryAdd);
            viewHolder.textInquiryNo = (TextView) convertView.findViewById(R.id.textInquiryNo);
            viewHolder.textPickupAdd = (TextView) convertView.findViewById(R.id.textPickupAdd);
            viewHolder.textVehicleModel = (TextView) convertView.findViewById(R.id.textVehicleModel);
            viewHolder.lblInquiryId = (TextView) convertView.findViewById(R.id.lblInquiryId);
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textName);
            viewHolder.textQuotedPrice = (TextView) convertView.findViewById(R.id.textQuotePrice);
            viewHolder.llQuotes = (LinearLayout) convertView.findViewById(R.id.llQuotes);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.lblInquiryId.setText(getItem(position).label);
        viewHolder.textDate.setText(getItem(position).anticipatedDate);
        viewHolder.textDeliveryAdd.setText(getItem(position).deliveryAddress);
        viewHolder.textInquiryNo.setText(getItem(position).orderNo);
        viewHolder.textPickupAdd.setText(getItem(position).pickupAddress);
        viewHolder.textVehicleModel.setText(getItem(position).vehicleModel);
        viewHolder.textName.setText(getItem(position).userName);
        viewHolder.textQuotedPrice.setText("$" + getItem(position).quote);

        if (getItem(position).quote.equals("")) {
            viewHolder.llQuotes.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.llQuotes.setVisibility(View.VISIBLE);
        }

//        viewHolder.textDeliveryAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, MapActivity.class);
//                intent.putExtra("lat", Double.parseDouble(getItem(position).deliveryLatitude));
//                intent.putExtra("long", Double.parseDouble(getItem(position).deliveryLongitude));
//                intent.putExtra("header", "Delivery Address");
//                intent.putExtra("hide", true);
//                context.startActivity(intent);
//            }
//        });
//
//        viewHolder.textPickupAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, MapActivity.class);
//                intent.putExtra("lat", Double.parseDouble(getItem(position).pickupLatitude));
//                intent.putExtra("long", Double.parseDouble(getItem(position).pickupLongitude));
//                intent.putExtra("header", "Pickup Address");
//                intent.putExtra("hide", true);
//                context.startActivity(intent);
//            }
//        });
        return convertView;
    }
}
