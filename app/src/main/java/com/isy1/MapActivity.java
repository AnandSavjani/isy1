package com.isy1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.isy1.adapter.PlaceAutocompleteAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class MapActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private TextView textHeader;
    private LinearLayout llSetAddress;
    private GoogleMap googleMap;
    private MarkerOptions marker;
    private EditText editLocation;
    private double lat, lng;
    private GoogleApiClient mGoogleApiClient;
    private Button btnDone;
    private AutoCompleteTextView autocompleteFragment;
    private String stateShortCode, stateLocation, zipCode, city;
    private PlaceAutocompleteAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initControl();
    }

    private void initControl() {
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(getIntent().getStringExtra("header"));

        btnDone = (Button) findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);

        editLocation = (EditText) findViewById(R.id.editLocation);
        llSetAddress = (LinearLayout) findViewById(R.id.llSetAddress);

        editLocation.setHint(getIntent().getStringExtra("header"));

        googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap)).getMap();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestFineAndCoarseLocation();
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        MapsInitializer.initialize(this);
        mGoogleApiClient.connect();

        googleMap.setMyLocationEnabled(true);
        if (utility.checkPlayServices()) {
            googleMap.getUiSettings().setZoomControlsEnabled(true);
        }

        if (getIntent().getBooleanExtra("hide", false)) {
            llSetAddress.setVisibility(View.GONE);
            lat = getIntent().getDoubleExtra("lat", 0);
            lng = getIntent().getDoubleExtra("long", 0);
            setUpMarker(lat, lng, true);
            if (utility.checkInternetConnection())
                setAddress(lat, lng);
        } else {
            googleMap.setOnMapClickListener(onMapClickListener);
            getCurrentLocation();
        }

        /*autocompleteFragment = (SupportPlaceAutocompleteFragment)
                getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);*/

        autocompleteFragment = (AutoCompleteTextView) findViewById(R.id.place_autocomplete_fragment);
//        autocompleteFragment.setText(address);

        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, null, null);
        autocompleteFragment.setAdapter(mAdapter);
        autocompleteFragment.setOnItemClickListener(mAutocompleteClickListener);

       /* autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i("Place: ", "" + place.getAddress() + place.getLatLng());
                setUpMarker(place.getLatLng().latitude, place.getLatLng().longitude, false);
                setAddress(place.getLatLng().latitude, place.getLatLng().longitude);
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
            }

            @Override
            public void onError(Status status) {
                Log.i("An error occurred: ", "" + status);
            }
        });

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT).build();
        autocompleteFragment.setFilter(typeFilter);

        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(12.0f);*/
    }


    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i("Tag", "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e("Tag", "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);
            setUpMarker(place.getLatLng().latitude, place.getLatLng().longitude, false);
            setAddress(place.getLatLng().latitude, place.getLatLng().longitude);
            lat = place.getLatLng().latitude;
            lng = place.getLatLng().longitude;

            // Format details of the place for display and show it in a TextView.
//            home_auto_address.setText(formatPlaceDetails(getResources(), place.getName(),
//                    place.getId(), place.getAddress(), place.getPhoneNumber(),
//                    place.getWebsiteUri()));

            hideSoftKeyboard();
            editLocation.setText(autocompleteFragment.getText().toString());

            // Display the third party attributions if set.
            places.release();

        }
    };

    private void setAddress(double latitude, double longitude) {
        try {
            String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true";
            Log.i("map url ", url);

            Geocoder geo = new Geocoder(MapActivity.this, Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
            if (addresses.isEmpty()) {
                editLocation.setText("Waiting for Location");
            } else {
                if (addresses.size() > 0) {
                    String address = checkForNull(addresses.get(0).getFeatureName()) + checkForNull(addresses.get(0).getSubLocality())
                            + checkForNull(addresses.get(0).getAdminArea()) + checkForNull(addresses.get(0).getCountryName());
                    address = address.substring(0, address.length() - 1);
                    zipCode = addresses.get(0).getPostalCode();
                    editLocation.setText(address);
                }
            }
            getShortName(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String checkForNull(String str) {
        if (str != null) {
            return str + ",";
        } else
            return "";
    }

    private void setUpMarker(double lat, double lng, boolean isZoom) {
        try {
            if (googleMap == null) {
                Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            } else {
                googleMap.clear();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(lat, lng)).zoom(12).bearing(90).tilt(30).build();

                marker = new MarkerOptions().position(new LatLng(lat, lng));
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                googleMap.addMarker(marker);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (utility.checkInternetConnection())
            setAddress(lat, lng);
    }

    public void hideSoftKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (StackOverflowError | Exception e) {
            e.printStackTrace();
        }

    }

    public void finishActivity(View view) {
        finish();
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestFineAndCoarseLocation();
        } else {
            if (utility.isLocationEnable()) {
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Location myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                lng = myLocation.getLongitude();
                lat = myLocation.getLatitude();
                setUpMarker(lat, lng, true);
            }
        }
    }

    //listeners

    GoogleMap.OnMapClickListener onMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng point) {
            lat = point.latitude;
            lng = point.longitude;
            googleMap.clear();
            googleMap.addMarker(new MarkerOptions().position(point));
            if (utility.checkInternetConnection())
                setAddress(lat, lng);
        }
    };

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnDone)) {
            Intent intent = new Intent();
            intent.putExtra("location", editLocation.getText().toString());
            intent.putExtra("latitude", lat);
            intent.putExtra("longitude", lng);
            intent.putExtra("statecode", stateShortCode);
            intent.putExtra("statelocation", stateLocation);
            intent.putExtra("zipcode", zipCode);
            intent.putExtra("city", city);
            setResult(3, intent);
            finish();
        }
    }

    private void getShortName(String url) {
        RequestQueue mVolleyQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    Log.i("location response ", response);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray arrayResult = jsonObject.getJSONArray("results");

                    JSONArray arrComponent = arrayResult.getJSONObject(0).getJSONArray("address_components");
                    for (int i = 0; i < arrComponent.length(); i++) {
                        JSONArray arrType = arrComponent.getJSONObject(i).getJSONArray("types");
                        for (int j = 0; j < arrType.length(); j++) {

                            if (arrType.getString(j).equals("administrative_area_level_1")) {
                                stateLocation = arrComponent.getJSONObject(i).getString("long_name");
                                stateShortCode = arrComponent.getJSONObject(i).getString("short_name");
                                Log.i("state location", stateLocation);
                                Log.i("state short name", stateShortCode);
                            }

                            if (arrType.getString(j).equals("administrative_area_level_2")) {
                                city = arrComponent.getJSONObject(i).getString("long_name");
                                Log.i("state location", city);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        stringRequest.setShouldCache(false);
        stringRequest.setTag("Volley");
        mVolleyQueue.add(stringRequest);
    }
}
