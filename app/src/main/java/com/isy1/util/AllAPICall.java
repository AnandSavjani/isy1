package com.isy1.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;

public class AllAPICall extends AsyncTask<String, String, String> {

    private onTaskComplete mOnTaskComplete;
    private GetJsonUrl jsonParser;
    private Context context;
    private Utility utility;
    private HashMap<String, String> pairListString;
    private HashMap<String, File> pairListFile;
    private String response;

    public AllAPICall(Context context, HashMap<String, String> pairListString, HashMap<String, File> pairListFile, onTaskComplete taskComplete) {
        this.mOnTaskComplete = taskComplete;
        this.context = context;
        utility = new Utility(context);
        this.pairListFile = pairListFile;
        if (pairListString != null)
            pairListString = SecurityUtils.setSecureParams(pairListString);
        this.pairListString = pairListString;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        jsonParser = new GetJsonUrl();
        utility.showProgressDialog();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.i("URL : ", params[0]);
        if (pairListString != null && pairListFile == null) {
            Log.i("params", pairListString.toString());
            response = jsonParser.getJSONResponseFromUrl(params[0], pairListString, null);
        } else if (pairListFile != null && pairListString != null) {
            Log.i("params", pairListString.toString() + " " + pairListFile.toString());
            response = jsonParser.getJSONResponseFromUrl(params[0], pairListString, pairListFile);
        } else
            response = jsonParser.getJSONResponseFromUrl(params[0], null, null);

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        utility.hideProgressDialog();
        if (result != null && !result.equals("")) {
            Log.i("response", result);
            mOnTaskComplete.onComplete(result);
        } else {
            Toast.makeText(context, "Something went wrong please try again later", Toast.LENGTH_SHORT).show();
        }
    }
}
