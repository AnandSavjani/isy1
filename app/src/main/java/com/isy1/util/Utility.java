package com.isy1.util;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.isy1.R;
import com.isy1.inquiry.InquiryDetailsActivity;
import com.isy1.inquiry.OrderDetailsActivity;
import com.isy1.notification.MessagesActivity;
import com.isy1.notification.NotificationListActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Utility {

    private Context context;
    private ProgressDialog dialog;

    public Utility(Context context) {
        this.context = context;
    }

    public String getTimeStamp() {
        return (DateFormat.format("ddMMyyyy_hhmmss", new java.util.Date()).toString());
    }

    public Bitmap setImageToImageView(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = options.outWidth, height_tmp = options.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeFile(filePath, o2);
    }

    /**
     * getDeviceId : return UNIQUE DeviceId
     */
    /* =================================================================== */
   /* public String getDeviceId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }*/
    public boolean validatePasswordLength(EditText editText) {
        if (TextUtils.isEmpty(editText.getText().toString())) {
            editText.setError("Password can't be blank.");
            editText.requestFocus();
            return false;
        } else if (editText.getText().length() < 6) {
            editText.setError("Password is too short, minimum 6 character length is required");
            editText.requestFocus();
            return false;
        } else
            return true;
    }

    /**
     * isValidPassword : return String if password is valid or null if not
     */
    /* =================================================================== */
    public boolean isValidPassword(EditText edtCurrentPassword, EditText edtRtypePassword) {
        String mCurrentPass = edtCurrentPassword.getText().toString().trim();
        String mRetpyePass = edtRtypePassword.getText().toString().trim();

        if (TextUtils.equals(mCurrentPass, mRetpyePass)) {
            return true;
        } else {
            edtRtypePassword.setError("Confirm Password doesn’t match.");
            edtRtypePassword.requestFocus();
            return false;
        }
    }


    public int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }

    public String addSignatureToGallery(Bitmap signature) {
        try {
            File photo = new File(new File(createDirectory()), getTimeStamp() + ".jpg");
            saveBitmapToJPG(signature, photo);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(photo);
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);
            return photo.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String createDirectory() {
        try {
            String root = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
            File createDir = new File(root + context.getResources().getString(R.string.app_name) + File.separator);
            if (!createDir.exists()) {
                createDir.mkdir();
            }
            return createDir.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        stream.close();
    }

    public boolean validateEditText(EditText editText) {
        if (!(TextUtils.isEmpty(editText.getText().toString()) || editText.getText() == null))
            return true;
        else {
            editText.requestFocus();
            return false;
        }
    }

    public boolean isValidEmail(EditText edtEmail) {
        if (edtEmail.getText().toString().length() != 0 && Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches())
            return true;
        else {
            edtEmail.requestFocus();
            return false;
        }
    }

    public void setupUI(View view) {
        final Activity activity = (Activity) context;
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public boolean checkInternetConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected())
            return true;
        else {
            Toast.makeText(context, context.getResources().getString(R.string.msg_internet_connection), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public String getTimeZone() {
        Calendar calendargmt = Calendar.getInstance();
        TimeZone mTimeZone = calendargmt.getTimeZone();
        int mGMTOffset = mTimeZone.getOffset(calendargmt.getTimeInMillis());
        return "" + TimeUnit.MINUTES.convert(mGMTOffset, TimeUnit.MILLISECONDS);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void sendNotification(String notiType, String msg, String orderId, String inquiryId) {

        NotificationManager notificationManager;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(context.getString(R.string.app_name));
        builder.setContentText(msg);
        Intent resultIntent;

        if (notiType.equalsIgnoreCase("Add New Inquiry") || notiType.equalsIgnoreCase("Edit Inquiry") ||
                notiType.equalsIgnoreCase("Cancel Inquiry")) {

            resultIntent = new Intent(context, InquiryDetailsActivity.class);
            resultIntent.putExtra("inquiryId", inquiryId);

        } else if (notiType.equalsIgnoreCase("Sent Quote")) {

            resultIntent = new Intent(context, InquiryDetailsActivity.class);
            resultIntent.putExtra("inquiryId", inquiryId);

        } else if (notiType.equalsIgnoreCase("Add New Order")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Delete Inquiry")) {

            resultIntent = new Intent(context, InquiryDetailsActivity.class);
            resultIntent.putExtra("inquiryId", inquiryId);

        } else if (notiType.equalsIgnoreCase("Edit Order")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_my_invoice));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Cancel Order")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Delete Order")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Assigned Carrier")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Vehicle Pickup")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Vehicle Delivery")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Invoice Sent")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Complete Payment Received")) {

            resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("iOrderId", orderId);
            resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
            resultIntent.putExtra("position", 4);

        } else if (notiType.equalsIgnoreCase("Admin Message")) {

            resultIntent = new Intent(context, MessagesActivity.class);
            resultIntent.putExtra("messageType", "all");
        } else
            resultIntent = new Intent(context, NotificationListActivity.class);

//        if (!orderId.equals("")) {
//            resultIntent = new Intent(context, OrderDetailsActivity.class);
//            resultIntent.putExtra("iOrderId", orderId);
//
//            if (notiType.equalsIgnoreCase("Invoice Sent")) {
//                resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_my_invoice));
//                resultIntent.putExtra("position", 2);
//            } else {
//                resultIntent.putExtra("header", context.getResources().getString(R.string.lbl_order_details));
//                resultIntent.putExtra("position", 2);
//            }
//            resultIntent.putExtra("from", "");
//        } else if (!inquiryId.equals("")) {
//            resultIntent = new Intent(context, InquiryDetailsActivity.class);
//            resultIntent.putExtra("inquiryId", inquiryId);
//        }

        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pending = PendingIntent.getActivity(context.getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.mipmap.ic_launcher);
            builder.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else
            builder.setSmallIcon(R.mipmap.ic_launcher);

        builder.setAutoCancel(true);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        builder.setAutoCancel(true);
        builder.setStyle(new Notification.BigTextStyle().bigText(msg));
        builder.setContentIntent(pending);
        Notification notification = builder.build();
        notificationManager.notify(1, notification);
    }

    /**
     * showProgressDialog : open dialog to show progress
     */

    public void showProgressDialog() {
        dialog = new ProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setMessage(context.getResources().getString(R.string.msg_please_wait));
        dialog.show();
    }

    /**
     * hideProgressDialog : dismiss dialog if already open
     */
    /* =================================================================== */
    public void hideProgressDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void showConfirmationDialog(String msg, final onAcceptListener acceptListener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = View.inflate(context, R.layout.dialog_confirmation, null);
        dialog.setContentView(view);
        TextView textView = (TextView) view.findViewById(R.id.textMessage);
        textView.setText(msg);
        view.findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                acceptListener.onAccept(true);
            }
        });

        view.findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (status != ConnectionResult.SUCCESS) {
            if (!GooglePlayServicesUtil.isUserRecoverableError(status)) {
                Toast.makeText(context, context.getResources().getString(R.string.msg_device_not_support), Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }

    public void setTextColor(TextView view, String fulltext, String subtext, int color) {
        view.setText(fulltext, TextView.BufferType.SPANNABLE);
        Spannable str = (Spannable) view.getText();
        int i = fulltext.indexOf(subtext);
        str.setSpan(new ForegroundColorSpan(context.getResources().getColor(color)), i + 1, fulltext.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        view.setText(str);
    }

    public void setHintColor(TextView view) {
        String styledText = view.getText().toString() + "<font color='red'>*</font>";
        view.setText(Html.fromHtml(styledText));
    }

    public void setHintColor(EditText view, String fulltext) {
        String styledText = fulltext + "<font color='red'>*</font>";
        view.setHint(Html.fromHtml(styledText));
    }

    /**
     * showOkDialog : It's a Warning or Information dialog.
     */
    /* =================================================================== */
    public void showOkDialog(String message) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(context.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton("OK", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
    }

    public Bitmap rotateImages(String imagePath) {
        ExifInterface ei;
        Bitmap bitmap;
        try {
            ei = new ExifInterface(imagePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            bitmap = setImageToImageView(imagePath);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return RotateBitmap(bitmap, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return RotateBitmap(bitmap, 180);
                default:
                    return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isLocationEnable() {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(myIntent);
            return false;
        } else {
            return true;
        }
    }

    private Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * hideSoftKeyboard : open keyboard if not open
     */
    /* =================================================================== */
    public void openSoftKeyboard(Activity activity, View editText) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(editText.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

}
