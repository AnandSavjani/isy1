package com.isy1.util;

import android.content.Context;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.EditText;


public class CustomSearchView extends EditText {

    private Context context;
    private AttributeSet attrs;
    private int defStyle;
    Utility utility;

    public CustomSearchView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init();
    }

    public CustomSearchView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        this.attrs = attrs;
        this.defStyle = defStyle;
        init();
    }

    private void init() {
        utility = new Utility(context);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "opensans_regular.ttf");
        this.setTypeface(font);
        this.setInputType(InputType.TYPE_CLASS_TEXT);
        this.setGravity(Gravity.CENTER_VERTICAL);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        tf = Typeface.createFromAsset(getContext().getAssets(), "opensans_regular.ttf");
        super.setTypeface(tf, style);
    }
}