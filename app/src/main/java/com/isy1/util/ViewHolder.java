package com.isy1.util;


import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ViewHolder {

    // inquiry row
    public TextView textDate, textInquiryNo, textVehicleModel, textPickupAdd, textDeliveryAdd, textQuotedPrice;
    public LinearLayout llQuotes;

    // notification row
    public TextView textNotifications;

    //order row
    public TextView textOrderNo, textStatus, textName;
    public ImageView imgStatus;

    //myinvoice row
    public TextView textViewInvoice, lblInquiryId;

}
