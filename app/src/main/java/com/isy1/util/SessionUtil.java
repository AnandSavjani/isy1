package com.isy1.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.isy1.R;

public class SessionUtil {
    private SharedPreferences objPreferences;
    private SharedPreferences.Editor objEditor;

    public SessionUtil(Context context) {
        objPreferences = context.getSharedPreferences(context.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        objEditor = objPreferences.edit();
    }

    /**
     * setBooleanDetail : set Boolean value into SP
     */
    public void setBooleanDetail(String key, Boolean value) {
        objEditor.putBoolean(key, value);
    }

    /**
     * getBooleanDetail : get Boolean value from SP
     */
    public boolean getBooleanDetail(String key) {
        return objPreferences.getBoolean(key, false);
    }

    /**
     * setIntDetail : set int value into SP
     */
    public void setIntDetail(String key, Boolean value) {
        objEditor.putBoolean(key, value);
    }

    /**
     * getIntDetail : get int value from SP
     */
    public int getIntDetail(String key) {
        return objPreferences.getInt(key, 0);
    }

    /**
     * setStringDetail : set String value into SP
     */
    public void setStringDetail(String key, String value) {
        objEditor.putString(key, value);
    }

    /**
     * getStringDetail : get String value from SP
     */
    public String getStringDetail(String key) {
        return objPreferences.getString(key, "");
    }

    /**
     * clearAllSP : clear your SP
     */
    public void clearAllSP() {
        objEditor.clear();
        objEditor.commit();
    }

    public void commitPref() {
        objEditor.apply();
    }

    public boolean isContain(String key) {
        return objPreferences.contains(key);
    }
}