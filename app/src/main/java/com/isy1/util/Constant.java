package com.isy1.util;


public interface Constant {

    int SPLASH_TIME = 3000;

    //weibo
    String WEIBO_APP_KEY = "3014039903";
    String WEIBO_APP_SECRET = "84c2629e69f00558e0234c896e54010e";
    String REDIRECT_URL = "http://www.sina.com";

    //GCM
    String Server_API_Key = "AIzaSyAq7H-k0egsuxZ5BRMYLVefmHhpr0Sby3k";

    String SENDER_ID = "453836886653";

    //Quickbook
    String QB_APP_TOKEN = "50952a42b797db4819b9462bad8620e2ee16";
    String QB_CONSUMER_KEY = "qyprdMoaeHiqit5keJpTRaWpdQzL48";
    String QB_CONSUMER_SECRET = "WO4ChONnOiHj5AvociIcOSWqDXxyUH0fawuTF7GW";

    String BASE_URL = "http://www.isy1transport.com/ISY1/api/v1/api/";

    String URL_MODEL_YEAR = BASE_URL + "vehiclemodel";
    String URL_PAGES = BASE_URL + "pages";
    String URL_SIGNUP = BASE_URL + "signup";
    String URL_LOGIN = BASE_URL + "login";
    String URL_FORGOT_PASSWORD = BASE_URL + "forgotpassword";
    String URL_FEEDBACK = BASE_URL + "feedback";
    String URL_EDIT_PROFILE = BASE_URL + "editprofile";
    String URL_CHANGE_PASSWORD = BASE_URL + "changepassword";
    String URL_COUNTRY = BASE_URL + "countrylist";
    String URL_INQUIRY = BASE_URL + "inquiries";
    String URL_INQUIRY_DETAILS = BASE_URL + "inquirydetail";
    String URL_MY_INQUIRIES = BASE_URL + "myinquiries";
    String URL_CONTACT_US = BASE_URL + "contactus";
    String URL_CONTRACT_AGREEMENT = BASE_URL + "contractagreement";
    String URL_ORDERS = BASE_URL + "orders";
    String URL_ORDER_DETAILS = BASE_URL + "orderdetail";
    String URL_MESSAGES = BASE_URL + "messages";
    String URL_DELIVERED = BASE_URL + "delivered";
    String URL_INVOICES = BASE_URL + "invoices";
    String URL_INVOICE_DETAIL = BASE_URL + "invoicedetail";
    String URL_DELIVERED_DETAIL = BASE_URL + "delivereddetail";
    String URL_MYHISTORY = BASE_URL + "myhistory";
    String URL_NOTIFICATIONS = BASE_URL + "notifications";
    String URL_PUSH_NOTIFICATION_ON_OFF = BASE_URL + "pushnotification";

}