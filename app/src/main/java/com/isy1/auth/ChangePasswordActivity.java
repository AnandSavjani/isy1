package com.isy1.auth;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    private EditText editCurrentPassword, editNewPassword, editConfirmPassword;
    private TextView textHeader, textChange;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initControl();
    }

    private void initControl() {
        editCurrentPassword = (EditText) findViewById(R.id.editCurrentPassword);
        editNewPassword = (EditText) findViewById(R.id.editNewPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);

        textHeader = (TextView) findViewById(R.id.textHeader);
        textChange = (TextView) findViewById(R.id.textChange);
        textHeader.setText(R.string.lbl_change_password);

        textChange.setOnClickListener(this);
    }

    public void finishActivity(View view) {
        finish();
    }

    public void validatePassword() {
        if (!validateText(editCurrentPassword)) {
            editCurrentPassword.setError("Please enter current password");
            editCurrentPassword.requestFocus();
        } else if (!utility.validatePasswordLength(editNewPassword)) {

        } else if (!validateText(editConfirmPassword)) {
            editConfirmPassword.setError("Please enter confirm password");
            editConfirmPassword.requestFocus();
        } else if (!utility.isValidPassword(editNewPassword, editConfirmPassword)) {
//            utility.showOkDialog("Password and confirm password is not match!");
        } else {
            changePasswordApi();
        }
    }

    void changePasswordApi() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("vPassword", getTextStr(editCurrentPassword));
        stringHashMap.put("newPassword", getTextStr(editNewPassword));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        Toast.makeText(ChangePasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        utility.showOkDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_CHANGE_PASSWORD);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textChange)) {
            validatePassword();
        }
    }
}
