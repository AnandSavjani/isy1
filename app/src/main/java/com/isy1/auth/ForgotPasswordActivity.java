package com.isy1.auth;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    private EditText editEmail;
    private TextView textHeader, textSend;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        initControl();
    }

    private void initControl() {
        textHeader = (TextView) findViewById(R.id.textHeader);
        textSend = (TextView) findViewById(R.id.textSend);
        textHeader.setText(R.string.txt_forgot_password);

        textSend.setOnClickListener(this);

        editEmail = (EditText) findViewById(R.id.editEmail);
    }

    private void sendMail() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("vEmailId", getTextStr(editEmail));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        Toast.makeText(ForgotPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        utility.showOkDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_FORGOT_PASSWORD);
    }

    public void finishActivity(View view) {
        finish();
    }

    void checkValidation() {
        if (utility.isValidEmail(editEmail)) {
            if (utility.checkInternetConnection())
                sendMail();
        } else {
            if(editEmail.getText().toString().isEmpty()){
                editEmail.setError("Email Address can't be blank.");
            }else
            editEmail.setError(getResources().getString(R.string.msg_valid_email_address));
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textSend)) {
            checkValidation();
        }
    }
}
