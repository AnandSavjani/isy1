package com.isy1.auth;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.MainActivity;
import com.isy1.R;
import com.isy1.WebviewActivity;
import com.isy1.bean.CountryBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private EditText editFirstName, editLastName, editContactNumber, editEmailAddress, editPassword,
            editConfirmPassword, editCountryCode;
    private CheckBox cbTc;
    private AutoCompleteTextView acCountry;
    private TextView textHeader, textTerms, textPolicy, textSignUp;
    private HashMap<String, String> stringHashMap;
    private ArrayList<CountryBean> arrCountryBean;
    private ArrayAdapter<String> stringArrayAdapter;
    private List<String> arrCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initControl();
    }

    private void initControl() {

        FirebaseMessaging.getInstance().subscribeToTopic("isy1");
        GCM_ID = FirebaseInstanceId.getInstance().getToken();
        Log.i("gcm id", GCM_ID);

        stringHashMap = new HashMap<>();
        arrCountryBean = new ArrayList<>();
        arrCountry = new ArrayList<>();

        stringArrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, arrCountry);

        textHeader = (TextView) findViewById(R.id.textHeader);
        textTerms = (TextView) findViewById(R.id.textTerms);
        textPolicy = (TextView) findViewById(R.id.textPolicy);
        textSignUp = (TextView) findViewById(R.id.textSignUp);
        textHeader.setText(R.string.btn_signup);

        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editContactNumber = (EditText) findViewById(R.id.editContactNumber);
        editEmailAddress = (EditText) findViewById(R.id.editEmailAddress);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);
        editCountryCode = (EditText) findViewById(R.id.editCountryCode);

        acCountry = (AutoCompleteTextView) findViewById(R.id.acCountry);
        acCountry.setThreshold(1);
        acCountry.setTypeface(Typeface.createFromAsset(getAssets(), "opensans_regular.ttf"));
        acCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                    editCountryCode.setText("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        acCountry.setOnItemClickListener(onItemClickListener);
        cbTc = (CheckBox) findViewById(R.id.cbTc);

        acCountry.setOnEditorActionListener(onEditorActionListener);

        Spannable wordtoSpan = new SpannableString(textPolicy.getText());
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), textPolicy.getText().length() - 1, textPolicy.getText().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textPolicy.setText(wordtoSpan);

        textTerms.setOnClickListener(this);
        textPolicy.setOnClickListener(this);
        textSignUp.setOnClickListener(this);

        if (utility.checkInternetConnection()) {
            new AllAPICall(this, null, null, new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        Gson gson = new Gson();
                        if (jsonObject.getInt("status") == 1) {
                            JSONArray arrResult = jsonObject.getJSONArray("result");
                            for (int i = 0; i < arrResult.length(); i++) {
                                CountryBean countryBean = gson.fromJson(arrResult.getJSONObject(i).toString(), CountryBean.class);
                                arrCountryBean.add(countryBean);
                                arrCountry.add(countryBean.countryName);
                            }
                            stringArrayAdapter.notifyDataSetChanged();
                            acCountry.setAdapter(stringArrayAdapter);
                        } else {
                            Toast.makeText(SignUpActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_COUNTRY);
        }

        acCountry.setText("USA");
        editCountryCode.setText("+1");

        editContactNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length() - editContactNumber.getSelectionStart();
                backspacingFlag = count > after;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {

                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length() >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true;
                        //here is the core. we substring the raw digits and add the mask as convenient
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6);
                        editContactNumber.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        editContactNumber.setSelection(editContactNumber.getText().length() - cursorComplement);

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = phone.substring(0, 3) + "-" + phone.substring(3);
                        editContactNumber.setText(ans);
                        editContactNumber.setSelection(editContactNumber.getText().length() - cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });
    }

    private void checkValidation() {
        if (!validateText(editFirstName)) {
            editFirstName.setError("First Name can't be blank");
        } else if (!validateText(editLastName)) {
            editLastName.setError("Last Name can't be blank");
        } else if (!validateText(acCountry)) {
            acCountry.setError("Please select country");
        } else if (!validateText(editCountryCode)) {
            acCountry.setError("Invalid Country.");
        } else if (!validateText(editContactNumber)) {
            editContactNumber.setError("Contact Number can't be blank.");
        } else if (!utility.isValidEmail(editEmailAddress)) {
            editEmailAddress.setError(getString(R.string.msg_valid_email_address));
        } else if (!utility.validatePasswordLength(editPassword)) {

        } else if (!validateText(editConfirmPassword)) {
            editConfirmPassword.setError("Please enter confirm password");
        } else if (!utility.isValidPassword(editPassword, editConfirmPassword)) {
            editConfirmPassword.setError("Confirm Password doesn’t match.");
        } else if (!cbTc.isChecked()) {
            utility.showOkDialog("You have to agree our Terms & Conditions and Privacy Policy.");
        } else {
            if (utility.checkInternetConnection())
                signUpCall();
        }
    }

    private void signUpCall() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("vFirstName", getTextStr(editFirstName));
        stringHashMap.put("vLastName", getTextStr(editLastName));
        stringHashMap.put("vCountryName", getTextStr(acCountry));
        stringHashMap.put("vCountryCode", getTextStr(editCountryCode));
        stringHashMap.put("vContactNumber", getTextStr(editContactNumber).replace("-", ""));
        stringHashMap.put("vEmailId", getTextStr(editEmailAddress));
        stringHashMap.put("vPassword", getTextStr(editPassword));
        stringHashMap.put("ePlatform", "A");
        stringHashMap.put("vDeviceToken", GCM_ID);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        Toast.makeText(SignUpActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        JSONObject objResult = jsonObject.getJSONObject("result");

                        sessionUtil.setStringDetail("iUserId", objResult.getString("iUserId"));
                        sessionUtil.setStringDetail("vFirstName", objResult.getString("vFirstName"));
                        sessionUtil.setStringDetail("vLastName", objResult.getString("vLastName"));
                        sessionUtil.setStringDetail("vCountryName", objResult.getString("vCountryName"));
                        sessionUtil.setStringDetail("vCountryCode", objResult.getString("vCountryCode"));
                        sessionUtil.setStringDetail("vContactNumber", objResult.getString("vContactNumber"));
                        sessionUtil.setStringDetail("vEmailId", objResult.getString("vEmailId"));
                        sessionUtil.commitPref();

                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        finish();
                    } else {
                        utility.showOkDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_SIGNUP);
    }

    public void finishActivity(View view) {
        finish();
    }

    //listeners

    @Override
    public void onClick(View v) {
        if (v.equals(textTerms)) {
            startActivity(new Intent(SignUpActivity.this, WebviewActivity.class).putExtra("pageid", "5"));
        } else if (v.equals(textPolicy)) {
            startActivity(new Intent(SignUpActivity.this, WebviewActivity.class).putExtra("pageid", "3"));
        } else if (v.equals(textSignUp)) {
            checkValidation();
        }
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String selection = (String) parent.getItemAtPosition(position);
            for (int i = 0; i < arrCountryBean.size(); i++) {
                if (arrCountryBean.get(i).countryName.equals(selection)) {
                    editCountryCode.setText(arrCountryBean.get(i).countryCode);
                    break;
                }
            }
        }
    };

    TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId,
                                      KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                editContactNumber.requestFocus();
                return true;
            }
            return false;
        }
    };
}
