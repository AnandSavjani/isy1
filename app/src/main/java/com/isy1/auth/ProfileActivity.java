package com.isy1.auth;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.bean.CountryBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.OpenGallery;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {

    private EditText editFirstName, editLastName, editMobileNo, editEmailAddress, editCountryCode;
    private AutoCompleteTextView acCountry;
    private TextView textHeader;
    private ImageView imgProfilePic, imgEdit, imgSearch;
    private File file;
    private TextView btnSubmit;
    private HashMap<String, String> stringHashMap;
    private HashMap<String, File> fileHashMap;
    private ArrayList<CountryBean> arrCountryBean;
    private ArrayAdapter<String> stringArrayAdapter;
    private List<String> arrCountry;
    private int flag = 0;
    static String imgPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initControl();
    }

    private void initControl() {
        fileHashMap = new HashMap<>();
        stringHashMap = new HashMap<>();

        arrCountryBean = new ArrayList<>();
        arrCountry = new ArrayList<>();

        stringArrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, arrCountry);

        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editMobileNo = (EditText) findViewById(R.id.editMobileNo);
        editEmailAddress = (EditText) findViewById(R.id.editEmailAddress);
        editCountryCode = (EditText) findViewById(R.id.editCountryCode);

        acCountry = (AutoCompleteTextView) findViewById(R.id.acCountry);
        acCountry.setThreshold(1);
        acCountry.setTypeface(Typeface.createFromAsset(getAssets(), "opensans_regular.ttf"));
        acCountry.setOnItemClickListener(onItemClickListener);
        acCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    editCountryCode.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgProfilePic = (ImageView) findViewById(R.id.imgProfilePic);
        imgProfilePic.setFocusable(true);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);
        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgSearch.setVisibility(View.VISIBLE);
        imgSearch.setImageResource(R.drawable.edit3);

        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(R.string.lbl_my_profile);

        imgEdit.setOnClickListener(this);
        imgSearch.setOnClickListener(this);

        btnSubmit = (TextView) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(ProfileActivity.this).clearDiskCache();
            }
        }).start();

        setEnableField(false);
        feelUpAllFields();
        getCountryList();
    }

    //methods

    private void getCountryList() {
        if (utility.checkInternetConnection()) {
            new AllAPICall(this, null, null, new onTaskComplete() {
                @Override
                public void onComplete(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        Gson gson = new Gson();
                        if (jsonObject.getInt("status") == 1) {
                            JSONArray arrResult = jsonObject.getJSONArray("result");
                            for (int i = 0; i < arrResult.length(); i++) {
                                CountryBean countryBean = gson.fromJson(arrResult.getJSONObject(i).toString(), CountryBean.class);
                                arrCountryBean.add(countryBean);
                                arrCountry.add(countryBean.countryName);
                            }
                            stringArrayAdapter.notifyDataSetChanged();
                            acCountry.setAdapter(stringArrayAdapter);
                        } else {
                            Toast.makeText(ProfileActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_COUNTRY);
        }
    }

    private void feelUpAllFields() {
        acCountry.dismissDropDown();
        acCountry.clearFocus();
        if (sessionUtil.isContain("iUserId")) {
            editFirstName.setText(sessionUtil.getStringDetail("vFirstName"));
            editLastName.setText(sessionUtil.getStringDetail("vLastName"));
            editMobileNo.setText(sessionUtil.getStringDetail("vContactNumber"));
            editEmailAddress.setText(sessionUtil.getStringDetail("vEmailId"));
            acCountry.setText(sessionUtil.getStringDetail("vCountryName"));
            editCountryCode.setText(sessionUtil.getStringDetail("vCountryCode"));

            if (!TextUtils.isEmpty(sessionUtil.getStringDetail("vProfilePic"))) {
                Glide.with(this).load(sessionUtil.getStringDetail("vProfilePic")).placeholder(R.drawable.placeholder).centerCrop().priority(Priority.HIGH).into(imgProfilePic);
            }
        }
    }

    public void finishActivity(View view) {
        onBackPressed();
    }

    private void setEnableField(boolean isEnable) {
        acCountry.dismissDropDown();
        flag = 0;
        if (isEnable) {
            textHeader.setText(R.string.lbl_edit_profile);
            btnSubmit.setVisibility(View.VISIBLE);
            imgEdit.setVisibility(View.VISIBLE);
            imgSearch.setVisibility(View.INVISIBLE);
        } else {
            textHeader.setText(R.string.lbl_my_profile);
            btnSubmit.setVisibility(View.GONE);
            imgSearch.setVisibility(View.VISIBLE);
            imgEdit.setVisibility(View.INVISIBLE);
        }
        editFirstName.setEnabled(isEnable);
        editLastName.setEnabled(isEnable);
        editMobileNo.setEnabled(isEnable);
        acCountry.setEnabled(isEnable);
    }

    private void checkValidation() {
        for (int i = 0; i < arrCountry.size(); i++) {
            if (arrCountryBean.get(i).countryName.equals(acCountry.getText().toString()) && arrCountryBean.get(i).countryCode.equals(editCountryCode.getText().toString())) {
                flag = 1;
                break;
            }
        }
        if (flag == 1) {
            editProfileApi();
        } else {
            utility.showOkDialog("Please select valid country");
        }
    }

    private void editProfileApi() {
        stringHashMap.clear();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("vFirstName", getTextStr(editFirstName));
        stringHashMap.put("vLastName", getTextStr(editLastName));
        stringHashMap.put("vCountryName", getTextStr(acCountry));
        stringHashMap.put("vCountryCode", getTextStr(editCountryCode));
        stringHashMap.put("vContactNumber", getTextStr(editMobileNo));
        stringHashMap.put("vEmailId", getTextStr(editEmailAddress));

        new AllAPICall(this, stringHashMap, fileHashMap, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        setEnableField(false);
                        Toast.makeText(ProfileActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        JSONObject objResult = jsonObject.getJSONObject("result");
                        sessionUtil.clearAllSP();

                        sessionUtil.setStringDetail("iUserId", objResult.getString("iUserId"));
                        sessionUtil.setStringDetail("vFirstName", objResult.getString("vFirstName"));
                        sessionUtil.setStringDetail("vLastName", objResult.getString("vLastName"));
                        sessionUtil.setStringDetail("vCountryName", objResult.getString("vCountryName"));
                        sessionUtil.setStringDetail("vCountryCode", objResult.getString("vCountryCode"));
                        sessionUtil.setStringDetail("vContactNumber", objResult.getString("vContactNumber"));
                        sessionUtil.setStringDetail("vEmailId", objResult.getString("vEmailId"));
                        sessionUtil.setStringDetail("vProfilePic", objResult.getString("vProfilePic"));
                        sessionUtil.commitPref();

                        feelUpAllFields();
                    } else {
                        utility.showOkDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_EDIT_PROFILE);
    }

    //listeners

    @Override
    public void onBackPressed() {

        if (editFirstName.isEnabled()) {
            setEnableField(false);
            feelUpAllFields();
            acCountry.dismissDropDown();
        } else {
            super.onBackPressed();
        }
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String selection = (String) parent.getItemAtPosition(position);
            for (int i = 0; i < arrCountryBean.size(); i++) {
                if (arrCountryBean.get(i).countryName.equals(selection)) {
                    editCountryCode.setText(arrCountryBean.get(i).countryCode);
                    break;
                }
            }
        }
    };

    @Override
    public void onClick(View v) {
        if (v.equals(imgEdit)) {
            startActivityForResult(new Intent(ProfileActivity.this, OpenGallery.class), 0);
        } else if (v.equals(imgSearch)) {
            setEnableField(true);
        } else if (v.equals(btnSubmit)) {
            if (utility.checkInternetConnection())
                checkValidation();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (!TextUtils.isEmpty(imgPath) || imgPath != null)
            Glide.with(this).load(new File(imgPath)).placeholder(R.drawable.placeholder).fitCenter().into(imgProfilePic);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fileHashMap.clear();
        if (resultCode == 1) {
            if (data.getStringExtra("picturepath") != null) {
                imgPath = data.getStringExtra("picturepath");
                file = new File(imgPath);
                if (data.getStringExtra("picturepath").contains("http"))
                    Glide.with(this).load(data.getStringExtra("picturepath")).centerCrop().priority(Priority.HIGH).into(imgProfilePic);
                else {
                    Glide.with(this).load(file).centerCrop().priority(Priority.HIGH).into(imgProfilePic);
                }
                fileHashMap.put("vProfilePic", file);
            } else {
                Toast.makeText(this, "Unable to fetch image.Please try another image.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}