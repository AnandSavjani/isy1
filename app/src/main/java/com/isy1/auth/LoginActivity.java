package com.isy1.auth;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.isy1.BaseActivity;
import com.isy1.MainActivity;
import com.isy1.R;
import com.isy1.inquiry.PostInquiryActivity;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText editEmailAddress, editPassword;
    private TextView textHeader, textForgotPassword, textLogin, textRegister, textQuickQuote;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initControl();
    }

    private void initControl() {
        FirebaseMessaging.getInstance().subscribeToTopic("isy1");
        GCM_ID = FirebaseInstanceId.getInstance().getToken();
        Log.i("gcm id", GCM_ID);

        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(R.string.lbl_login);

        editEmailAddress = (EditText) findViewById(R.id.editEmailAddress);
        editPassword = (EditText) findViewById(R.id.editPassword);
        findViewById(R.id.imgBack).setVisibility(View.INVISIBLE);

        textForgotPassword = (TextView) findViewById(R.id.textForgotPassword);
        textLogin = (TextView) findViewById(R.id.textLogin);
        textRegister = (TextView) findViewById(R.id.textRegister);
        textQuickQuote = (TextView) findViewById(R.id.textQuickQuote);

        textForgotPassword.setOnClickListener(this);
        textLogin.setOnClickListener(this);
        textRegister.setOnClickListener(this);
        textQuickQuote.setOnClickListener(this);
    }

    // methods

    public void checkLogin() {

        if (editEmailAddress.getText().toString().isEmpty()) {
            editEmailAddress.setError("Email Address Can't be blank.");
        } else if (!utility.isValidEmail(editEmailAddress)) {
            editEmailAddress.setError(getString(R.string.msg_valid_email_address));
        } else if (!utility.validateEditText(editPassword)) {
            editPassword.setError(getString(R.string.msg_enter_password));
        } else {
            if (utility.checkInternetConnection())
                callLoginApi();
        }
    }

    private void callLoginApi() {
        stringHashMap = new HashMap<>();
        stringHashMap.put("vEmailId", getTextStr(editEmailAddress));
        stringHashMap.put("vPassword", getTextStr(editPassword));
        stringHashMap.put("ePlatform", "A");
        stringHashMap.put("vDeviceToken", GCM_ID);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        JSONObject objResult = jsonObject.getJSONObject("result");

                        sessionUtil.setStringDetail("iUserId", objResult.getString("iUserId"));
                        sessionUtil.setStringDetail("vFirstName", objResult.getString("vFirstName"));
                        sessionUtil.setStringDetail("vLastName", objResult.getString("vLastName"));
                        sessionUtil.setStringDetail("vCountryName", objResult.getString("vCountryName"));
                        sessionUtil.setStringDetail("vCountryCode", objResult.getString("vCountryCode"));
                        sessionUtil.setStringDetail("vContactNumber", objResult.getString("vContactNumber"));
                        sessionUtil.setStringDetail("vEmailId", objResult.getString("vEmailId"));
                        sessionUtil.setStringDetail("vProfilePic", objResult.getString("vProfilePic"));
                        sessionUtil.commitPref();

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    } else {
                        utility.showOkDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_LOGIN);
    }

    public void finishActivity(View view) {
        finish();
    }

    // listeners

    @Override
    public void onClick(View v) {
        if (v.equals(textForgotPassword)) {
            startActivity(new Intent(this, ForgotPasswordActivity.class));
        } else if (v.equals(textLogin)) {
            checkLogin();
        } else if (v.equals(textRegister)) {
            startActivity(new Intent(this, SignUpActivity.class));
        } else if (v.equals(textQuickQuote)) {
            startActivity(new Intent(this, PostInquiryActivity.class).putExtra("header", getResources().getString(R.string.btn_quick_inquiry)));
        }
    }
}
