package com.isy1;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.isy1.auth.LoginActivity;
import com.isy1.inquiry.DeliveryFragment;
import com.isy1.inquiry.InquiryFragment;
import com.isy1.inquiry.MyInquiriesActivity;
import com.isy1.inquiry.MyInvoiceActivity;
import com.isy1.inquiry.OrderFragment;
import com.isy1.notification.MessagesActivity;
import com.isy1.notification.NotificationListActivity;
import com.isy1.setting.SettingActivity;
import com.isy1.util.NonSwipeableViewPager;
import com.isy1.util.onAcceptListener;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private NonSwipeableViewPager viewPager;
    private DrawerLayout drawerLayout;
    private RelativeLayout leftDrawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private TextView textHome, textMyInquiry, textMyHistory, textSettings, textLogout, textHeader,
            textMyMessage, textMyInvoice, textName, textLocation;
    private PagerSlidingTabStrip tabs;
    private FragmentPagerMainAdapter fragmentStatePagerAdapter;
    private LinearLayout mTabsLinearLayout;
    private ImageView imgSearch, imgNotification;
    private SearchView searchView;
    private ImageView imgProfilePic, closeButton;
    private ScrollView svDrawer;
    boolean doubleBackToExitPressedOnce = false;
    private InquiryFragment mInquiryFragment;
    private OrderFragment mOrderFragment;
    private DeliveryFragment mDeliveryFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initControl();

    }

    private void initControl() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        leftDrawer = (RelativeLayout) findViewById(R.id.leftDrawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        svDrawer = (ScrollView) findViewById(R.id.svDrawer);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                svDrawer.fullScroll(ScrollView.FOCUS_UP);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                utility.hideSoftKeyboard(MainActivity.this);
                if (sessionUtil.isContain("iUserId")) {
                    textName.setText(sessionUtil.getStringDetail("vFirstName").toUpperCase() + " " + sessionUtil.getStringDetail("vLastName").toUpperCase());
                    textLocation.setText(sessionUtil.getStringDetail("vCountryName"));
                }

                if (!TextUtils.isEmpty(sessionUtil.getStringDetail("vProfilePic"))) {
                    Glide.with(MainActivity.this).load(sessionUtil.getStringDetail("vProfilePic")).placeholder(R.drawable.placeholder).centerCrop().priority(Priority.HIGH).into(imgProfilePic);
                }
            }
        };

        drawerLayout.addDrawerListener(mActionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });
        getSupportActionBar().setTitle("");

        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setQueryHint(getResources().getString(R.string.hint_search_here));
        searchView.setOnQueryTextListener(onQueryTextListener);
        int searchCloseButtonId = searchView.getResources().getIdentifier("android:id/search_close_btn", null, null);
        closeButton = (ImageView) searchView.findViewById(searchCloseButtonId);
        closeButton.setOnClickListener(this);

        imgProfilePic = (ImageView) findViewById(R.id.imgProfilePic);

        textHeader = (TextView) findViewById(R.id.textHeader);
        textHome = (TextView) findViewById(R.id.textHome);
        textMyInquiry = (TextView) findViewById(R.id.textMyInquiry);
        textMyInvoice = (TextView) findViewById(R.id.textMyInvoice);
        textMyHistory = (TextView) findViewById(R.id.textMyHistory);
        textSettings = (TextView) findViewById(R.id.textSettings);
        textLogout = (TextView) findViewById(R.id.textLogout);
        textMyMessage = (TextView) findViewById(R.id.textMyMessage);
        textName = (TextView) findViewById(R.id.textName);
        textLocation = (TextView) findViewById(R.id.textLocation);
        textHeader.setText(R.string.lbl_home);

        textHome.setOnClickListener(this);
        textMyInquiry.setOnClickListener(this);
        textMyHistory.setOnClickListener(this);
        textSettings.setOnClickListener(this);
        textLogout.setOnClickListener(this);
        textMyMessage.setOnClickListener(this);
        textMyInvoice.setOnClickListener(this);

        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgNotification = (ImageView) findViewById(R.id.imgNotification);

        imgSearch.setOnClickListener(this);
        imgNotification.setOnClickListener(this);

        fragmentStatePagerAdapter = new FragmentPagerMainAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.main_activity));
        viewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager);
        assert viewPager != null;
        viewPager.setAdapter(fragmentStatePagerAdapter);

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(viewPager);
        tabs.setTextColor(R.color.color_black);

        mTabsLinearLayout = ((LinearLayout) tabs.getChildAt(0));
        changeTabColor(0);

        tabs.setOnPageChangeListener(onPageChangeListener);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(leftDrawer)) {
            drawerLayout.closeDrawer(leftDrawer);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    private void changeTabColor(int position) {
        for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
            if (i == position) {
                tv.setTextColor(Color.WHITE);
                tv.setBackgroundColor(ContextCompat.getColor(this, R.color.color_green));
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.uparrow);
            } else {
                tv.setTextColor(Color.BLACK);
                tv.setBackgroundColor(ContextCompat.getColor(this, R.color.color_light_gray));
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
    }

    private void searchFunctionality(String query) {
        if (mInquiryFragment != null && viewPager.getCurrentItem() == 0) {
            mInquiryFragment.searchForInquiry(query);
        } else if (mOrderFragment != null && viewPager.getCurrentItem() == 1) {
            mOrderFragment.searchForOrder(query);
        } else if (mDeliveryFragment != null && viewPager.getCurrentItem() == 2) {
            mDeliveryFragment.searchForDelivered(query);
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(final View v) {
        drawerLayout.closeDrawer(leftDrawer);
        drawerLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (v.equals(textSettings)) {
                    startActivity(new Intent(MainActivity.this, SettingActivity.class));
                } else if (v.equals(textHome)) {
                    onResume();
                } else if (v.equals(textLogout)) {
                    utility.showConfirmationDialog("Are you sure you want to Logout ?", new onAcceptListener() {
                        @Override
                        public void onAccept(boolean isTrue) {
                            sessionUtil.clearAllSP();
                            startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }
                    });
                } else if (v.equals(textMyInquiry)) {
                    startActivity(new Intent(MainActivity.this, MyInquiriesActivity.class)
                            .putExtra("url", URL_MY_INQUIRIES)
                            .putExtra("header", getResources().getString(R.string.lbl_my_inquiries)));
                } else if (v.equals(textMyInvoice)) {
                    startActivity(new Intent(MainActivity.this, MyInvoiceActivity.class));
                } else if (v.equals(textMyHistory)) {
                    startActivity(new Intent(MainActivity.this, MyInquiriesActivity.class)
                            .putExtra("url", URL_MYHISTORY)
                            .putExtra("header", getResources().getString(R.string.lbl_my_history)));
                } else if (v.equals(imgSearch)) {
                    if (searchView.isShown()) {
                        searchView.clearFocus();
                        searchView.setQuery("", false);
                        searchView.setVisibility(View.GONE);
                    } else {
                        searchView.setVisibility(View.VISIBLE);
                        utility.openSoftKeyboard(MainActivity.this, searchView);
                        searchView.requestFocusFromTouch();
                        searchView.setIconified(false);
                        searchView.setFocusable(true);
                    }
                } else if (v.equals(textMyMessage)) {
                    Intent intent = new Intent(MainActivity.this, MessagesActivity.class);
                    intent.putExtra("messageType", "all");
                    startActivity(intent);
                } else if (v.equals(imgNotification)) {
                    startActivity(new Intent(MainActivity.this, NotificationListActivity.class));
                } else if (v.equals(closeButton)) {
                    searchView.setQuery("", true);
                    searchFunctionality("");
                }
            }
        }, 200);
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            searchView.setQuery("", false);
            changeTabColor(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            searchView.clearFocus();
            searchFunctionality(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };

    public class FragmentPagerMainAdapter extends FragmentPagerAdapter {

        private String title[];

        public FragmentPagerMainAdapter(FragmentManager fm, String title[]) {
            super(fm);
            this.title = title;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                mInquiryFragment = InquiryFragment.newInstance(position);
                return mInquiryFragment;
            } else if (position == 1) {
                mOrderFragment = OrderFragment.newInstance(position);
                return mOrderFragment;
            } else if (position == 2) {
                mDeliveryFragment = DeliveryFragment.newInstance(position);
                return mDeliveryFragment;
            } else
                return null;
        }

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }
    }
}
