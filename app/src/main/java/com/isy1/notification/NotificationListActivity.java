package com.isy1.notification;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.adapter.NotificationAdapter;
import com.isy1.bean.NotificationBean;
import com.isy1.inquiry.InquiryDetailsActivity;
import com.isy1.inquiry.OrderDetailsActivity;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NotificationListActivity extends BaseActivity {

    private ListView lvNotification;
    private NotificationAdapter notificationAdapter;
    private TextView textHeader;
    private HashMap<String, String> stringHashMap;
    private List<NotificationBean> listNotification;
    int nextPos, flag = 0;
    private Gson gson;
    private TextView textNoResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_invoice);
        initControl();
    }

    private void initControl() {
        gson = new Gson();
        listNotification = new ArrayList<>();

        textHeader = (TextView) findViewById(R.id.textHeader);
        textNoResult = (TextView) findViewById(R.id.textNoResult);
        textHeader.setText(R.string.lbl_notification);

        lvNotification = (ListView) findViewById(R.id.lvInvoice);
        notificationAdapter = new NotificationAdapter(this, listNotification);

        lvNotification.setAdapter(notificationAdapter);
        lvNotification.setOnItemClickListener(onItemClickListener);
        lvNotification.setOnScrollListener(onScrollListener);

        if (utility.checkInternetConnection()) {
            listNotification.clear();
            nextPos = 0;
            getNotification(0);
        }
    }

    public void finishActivity(View view) {
        finish();
    }

    private void getNotification(final int pageStart) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("pageStart", "" + pageStart);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    if (jsonObject.getInt("status") == 1) {
                        flag = 0;
                        nextPos = nextPos + 10;
                        JSONArray arrResult = jsonObject.getJSONArray("result");
                        for (int i = 0; i < arrResult.length(); i++) {
                            listNotification.add(gson.fromJson(arrResult.getJSONObject(i).toString(), NotificationBean.class));
                        }
                        lvNotification.setSelectionFromTop(pageStart - 1, 0);
                        lvNotification.setVisibility(View.VISIBLE);
                        textNoResult.setVisibility(View.GONE);
                    } else {
                        flag = -1;
                        if (listNotification.isEmpty()) {
                            lvNotification.setVisibility(View.GONE);
                            textNoResult.setVisibility(View.VISIBLE);
                        }
                    }
                    notificationAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_NOTIFICATIONS);
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = null;
            if (!listNotification.get(position).inquiryId.equals("")) {
                intent = new Intent(NotificationListActivity.this, InquiryDetailsActivity.class);
                intent.putExtra("inquiryId", listNotification.get(position).inquiryId);
            }
            if (!listNotification.get(position).orderId.equals("")) {
                intent = new Intent(NotificationListActivity.this, OrderDetailsActivity.class);
                intent.putExtra("iOrderId", listNotification.get(position).orderId);
                intent.putExtra("header", "Order Details");
                intent.putExtra("from", "");
            }
            startActivity(intent);

        }
    };

    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE && lvNotification.getLastVisiblePosition() >= nextPos - 1 && flag != -1) {
                if (utility.checkInternetConnection()) {
                    getNotification(nextPos);
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };
}