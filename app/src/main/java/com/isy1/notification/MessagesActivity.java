package com.isy1.notification;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.isy1.BaseActivity;
import com.isy1.R;
import com.isy1.adapter.MessageAdapter;
import com.isy1.bean.MessageBean;
import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MessagesActivity extends BaseActivity {

    private ListView lvMessage;
    private TextView textHeader;
    private MessageAdapter messageAdapter;
    private ImageView imgSearch;
    private EditText searchView;
    private HashMap<String, String> stringHashMap;
    private String strSearch = "", messageType = "all";
    private List<MessageBean> listMessage;
    private int nextPos, flag = 0;
    private TextView textNoResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_invoice);
        initControl();
    }

    private void initControl() {
        listMessage = new ArrayList<>();
        messageType = getIntent().getStringExtra("messageType");

        textHeader = (TextView) findViewById(R.id.textHeader);
        textNoResult = (TextView) findViewById(R.id.textNoResult);

        messageAdapter = new MessageAdapter(this, listMessage);

        searchView = (EditText) findViewById(R.id.searchView);
        searchView.setOnEditorActionListener(onEditorActionListener);
        searchView.setOnTouchListener(touchListener);

        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgSearch.setVisibility(View.VISIBLE);
        imgSearch.setOnClickListener(onClickListener);

        lvMessage = (ListView) findViewById(R.id.lvInvoice);
        lvMessage.setAdapter(messageAdapter);
        lvMessage.setOnScrollListener(onScrollListener);

        if (messageType.equals("order")) {
            textHeader.setText(R.string.lbl_message);
            imgSearch.setVisibility(View.INVISIBLE);
        } else {
            textHeader.setText(R.string.lbl_my_message);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (utility.checkInternetConnection()) {
            listMessage.clear();
            getMessageAPICall(0);
            nextPos = 0;
        }
    }

    public void finishActivity(View view) {
        onBackPressed();
    }

    private void getMessageAPICall(final int pageStart) {
        stringHashMap = new HashMap<>();

        stringHashMap.put("iUserId", sessionUtil.getStringDetail("iUserId"));
        stringHashMap.put("messageType", messageType);
        stringHashMap.put("pageStart", "" + pageStart);
        stringHashMap.put("timeZoneOffset", utility.getTimeZone());
        stringHashMap.put("search", strSearch);

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Gson gson = new Gson();
                    if (jsonObject.getInt("status") == 1) {
                        flag = 0;
                        nextPos = nextPos + 10;
                        JSONArray arrResult = jsonObject.getJSONArray("result");
                        for (int i = 0; i < arrResult.length(); i++) {
                            listMessage.add(gson.fromJson(arrResult.getJSONObject(i).toString(), MessageBean.class));
                        }
                        textNoResult.setVisibility(View.GONE);
                        lvMessage.setVisibility(View.VISIBLE);
                    } else {
                        flag = -1;
                        if (listMessage.isEmpty()) {
                            textNoResult.setVisibility(View.VISIBLE);
                            lvMessage.setVisibility(View.GONE);
                        }
                    }
                    messageAdapter.notifyDataSetChanged();
                    lvMessage.setSelectionFromTop(pageStart - 1, 0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_MESSAGES);
    }

    //listeners

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (searchView.isShown()) {
                searchView.setVisibility(View.GONE);
                textHeader.setVisibility(View.VISIBLE);
                utility.hideSoftKeyboard(MessagesActivity.this);
                searchView.setText("");
            } else {
                searchView.setVisibility(View.VISIBLE);
                textHeader.setVisibility(View.GONE);
                utility.openSoftKeyboard(MessagesActivity.this, searchView);
                searchView.requestFocus();
            }
        }
    };
    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE && lvMessage.getLastVisiblePosition() >= nextPos - 1 && flag != -1) {
                if (utility.checkInternetConnection()) {
                    getMessageAPICall(nextPos);
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        }
    };

    TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                utility.hideSoftKeyboard(MessagesActivity.this);
                strSearch = v.getText().toString();
                onResume();
            }
            return false;
        }
    };

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_RIGHT = 2;
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (searchView.getRight() - searchView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    strSearch = "";
                    onResume();
                    searchView.setText("");
                    utility.hideSoftKeyboard(MessagesActivity.this);
                    searchView.clearFocus();
                    return true;
                }
            }
            return false;
        }
    };
}
