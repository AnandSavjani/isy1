package com.isy1;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.isy1.util.AllAPICall;
import com.isy1.util.onTaskComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class WebviewActivity extends BaseActivity {

    private TextView textHeader, textData;
    private HashMap<String, String> stringHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        initControl();
    }

    private void initControl() {
        textHeader = (TextView) findViewById(R.id.textHeader);
        textData = (TextView) findViewById(R.id.textData);
        stringHashMap = new HashMap<>();

        stringHashMap.put("iPageId", getIntent().getStringExtra("pageid"));

        new AllAPICall(this, stringHashMap, null, new onTaskComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getInt("status") == 1) {
                        JSONObject objResult = jsonObject.getJSONObject("result");
                        textHeader.setText(objResult.getString("vPageName"));
                        textData.setText(Html.fromHtml(objResult.getString("txContent")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL_PAGES);
    }

    public void finishActivity(View view) {
        finish();
    }
}
